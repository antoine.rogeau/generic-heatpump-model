# Generic HeatPump Model
[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://gitlab.com/antoine.rogeau)
[![ForTheBadge powered-by-electricity](http://ForTheBadge.com/images/badges/powered-by-electricity.svg)](http://ForTheBadge.com)


[![Open Source Love svg3](https://badges.frapsoft.com/os/v3/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![DOI:10.1016/j.enbuild.2024.114490](https://zenodo.org/badge/DOI/10.1016/j.enbuild.2024.114490.svg)](https://doi.org/10.1016/j.enbuild.2024.114490)
[![DOI:10.5281/zenodo.10529245](https://zenodo.org/badge/DOI/10.5281/zenodo.10529245.svg)](https://doi.org/10.5281/zenodo.10529245)
[![DOI:10.5281/zenodo.10594143](https://zenodo.org/badge/DOI/10.5281/zenodo.10594143.svg)](https://doi.org/10.5281/zenodo.10594143)

## Description of the project
This project contains the code corresponding to the journal article "A generic methodology for mapping the performances of various heat pumps configurations considering part-load behaviour", published in _Energy and Buildings_ on 2024, September 01. (https://doi.org/10.1016/j.enbuild.2024.114490)

The datasets resulting from this code can be found on the following Zenodo repository: https://doi.org/10.5281/zenodo.10529245

It contains all the code necessary to run estimates of the performances of heat pumps in both heating and cooling modes.
Multiple technologies are implemented, including:
- Air-to-air / Air-to-water / Water-to-water systems
- Single speed / Multi-speed / Inverter driven technologies
- Monovalent / Bivalent parallel / Bivalent alternative operating modes

## Content of the repository
<pre>
+-- Generic_Heatpump_Modeling
|   +-- Mapping                     <b>Code necessary to perform mapping of performances over Europe</b>
|   |   +-- Aggregate_NUTS.py       *Aggregating data at higher levels (NUTS) after kriging
|   |   +-- kriging_DHU.py          *Kriging of weather indicators (degree days and peak temperature)
|   |   +-- kriging_performances.py *Kriging of performance indicators (SCOP/SEER, Peak COP/EER, Shares)
|   |   `-- simulate_TMY.py         *Simulate the performances of HPs at all the TMY points
|   +-- Performances comparison     <b>Code to estimate the performances of differents systems in a single location (for plots in paper)</b>
|   |   +-- compare_systems.py      *Compare all the configurations system/technology/mode
|   |   +-- effect_PartLoad.py      *Compares the effects of part-load modeling
|   |   `-- effect_oversizing.py    *Compares the effect of oversizing on performances
|   +-- Case study                  <b>Code for the study case of the retrofit order (see paper)</b>
|   |   `-- Case_study.py           *Compares the various configuration with oversizing and temperature adaptation
|   +-- functions.py                *All the functions necessary for the runs
|   `-- map_TMY.py                  *Estimates the peak temperatures at the TMY points
+-- data
|   +-- countries_border.*          *Shapefile of the countries borders of Europe
|   `-- simple_chauffage.csv        *Considered technologies input
+-- environment.txt
`-- README.md
</pre>


## Installation
### Clone the repository
  ```
  git clone https://gitlab.com/antoine.rogeau/generic-heatpump-model.git
  ```

### Environment installation
  * Create a new environment and activate it directly:
  ```
  py -m venv .venv
  .venv/Scripts/activate
  ```

## Packages Installation
Now, we are ready to install the necessary packages to start working on the project. To do so, please run the following commands in a prompt.
  ```
  py -m pip install -r environment.txt
  ```

## Authors
- Antoine Rogeau, _Mines Paris PSL_ & _ENGIE Impact France_