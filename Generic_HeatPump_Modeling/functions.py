import math
import numpy as np
import pandas as pd
import datetime

### Set the fluid temperatures for heating
TemperatureValues = {
    "Fan coil unit": 20,
    "FloorHeating": 35,
    "RadiatorLT": 45,
    "RadiatorMT": 55,
    "RadiatorHT": 65,
    "radiateurTHT": 75}

### Set the fluid temperatures for cooling
TemperatureValuesCooling = {
    "Fan coil unit": 24,
    "FloorHeating": 15,
    "RadiatorLT": np.nan,
    "RadiatorMT": np.nan,
    "RadiatorHT": np.nan,
    "radiateurTHT": np.nan}

# coefficients from this PhD Thesis https://pastel.archives-ouvertes.fr/tel-02969503/document
# equation (3.7) (3.8) p78
# Adjusted for A/A Hp from this source https://www.researchgate.net/publication/323788940_A_Dual-Heat-Pump_Residential_Heating_System_for_Shaping_Electric_Utility_Load
new_nominal_COP_curve_Coefficients = {
    "A/A HP": {"a1": 3.9747, "b1": -0.121, "c1": 0.0011, "d1": 0,
               "a2": 3.9747, "b2": -0.121, "c2": 0.0011},
    "A/W HP": {"a1": 5.6, "b1": -0.09, "c1": 0.0005, "d1": 0,
               "a2": 5.6, "b2": -0.09, "c2": 0.0005},
    "W/W HP": {"a1": 10.29, "b1": -0.21, "c1": 0.0012, "d1": 0,
               "a2": 10.29, "b2": -0.21, "c2": 0.0012}
}

### Parload coefficients for Inverter-driven phase
Inverter_partload_Coefficients = {
    "v1": {"PLF_biv": 1.4, "PLR_min": 0.3, "name": "Filliard et al. (2009)"},
    # Based on https://hal-mines-paristech.archives-ouvertes.fr/hal-00574663/document
    "v2": {"a1": 1.646, "a2": -3.588, "a3": 2.932, "PLR_min": 0.25, "name": "Toffanin et al. (2019)"}
    # Based on http://www.ibpsa.org/proceedings/BS2019/BS2019_210611.pdf
}

### Parload coefficients for on/off phase
OnOff_partload_Coefficients = {
    # Based on https://www.rehva.eu/fileadmin/REHVA_Journal/REHVA_Journal_2016/RJ_issue_4/p.45/45-49_RJ1604_WEB.pdf
    "v1": {"Cd": 0, "Cc": 0.7, "name": "ENI14825"},  # ENI14825
    "v2": {"Cd": 0.2, "Cc": 0.9, "name": "Fuentes et al. (2016)"},  # Fuentes et al (2016)
    "v3": {"Ce": 0.25, "name": "ASHRAE 116-1995"}  # ARI210/240 ,ASHRAE 116-1995
}

### for Bi-compressor, the first compressor is assumed to provide 70% of the peak
BiCompressor_partload_Coefficients = {
    "v1": {"PLR_min": 0.7}
}

### Impact of the defrost on performances based on https://www.sciencedirect.com/science/article/pii/S2352710222017739
DefrostImpact = {
    "a": 15.05, "b": -0.2543, "c": 0.01351, "d": -0.007022, "e": 0.4319, "f": -0.005945, "g": 0.03681
}


def estimate_impact_defrost(meteo_data, consider_defrost_equation=True):
    """
    When needed, the defrost reduces the performances of the heat pump. This function calculates the deterioration based
    the relative humidity and temperature, as presented by
    Roccatello et al. (https://www.sciencedirect.com/science/article/pii/S2352710222017739)

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param consider_defrost_equation: do we want to consider the effect ot not
    :return: the meteo_data object with an additional columns accounting for the deterioration (DOF)
    """
    if consider_defrost_equation:
        global DefrostImpact

        limit_lower_rh = meteo_data["rh"] < 0.7
        limit_lower_temp = meteo_data["temp"] < -4
        limit_upper_temp = meteo_data["temp"] > 3

        DOF = DefrostImpact["a"] + DefrostImpact["b"] * meteo_data["rh"] + DefrostImpact["c"] * meteo_data["temp"] + \
              DefrostImpact["d"] * meteo_data["rh"] * meteo_data["temp"] + \
              DefrostImpact["e"] * meteo_data["temp"] ** 2 + DefrostImpact["f"] * meteo_data["rh"] * meteo_data[
                  "temp"] ** 2 + DefrostImpact["g"] * meteo_data["temp"] ** 3

        DOF.loc[limit_lower_rh] = 0
        DOF.loc[limit_upper_temp] = 0

        max_DOF = meteo_data["DOF"] = DefrostImpact["a"] + DefrostImpact["b"] * meteo_data["rh"] + DefrostImpact[
            "c"] * (-4) + DefrostImpact["d"] * meteo_data["rh"] * (-4) + \
                                      DefrostImpact["e"] * (-4) ** 2 + DefrostImpact["f"] * meteo_data["rh"] * (
                                          -4) ** 2 + DefrostImpact["g"] * (-4) ** 3

        DOF.loc[limit_lower_temp] = max_DOF

        meteo_data["DOF"] = DOF / 100
    else:
        meteo_data["DOF"] = 0


def estimate_heating_cooling_capacity(in_meteo_data, in_params, in_meteo_data_forsizing, sized_for, share_peak=0):
    """
    The heating and cooling capacities of a heat pump varies with both the source and sink temperatures.
    Based on the work of Bryne et al. (https://link.springer.com/article/10.1007/s12273-012-0089-0), this function
    estimates the heating and cooling capacities at each moment of the time serie

    :param in_meteo_data: weather data time serie, including relative humidity and temperature
    :param in_params: input parameters of the simulated system
    :param in_meteo_data_forsizing: the system is sized for a specific moment (base temperature) which will define the heating capacisy of the system
    :param sized_for: is the system sized for heating or for cooling ?
    :param share_peak: if the share is over zero, the HP is not meant to satisfy all the heating need at base temperature
    :return: the meteo_data object with an additional columns accounting for the heating (and cooling) capacity HC/CC
    """

    if sized_for == "Heating":
        T_fluid_peak = in_meteo_data_forsizing["T_fluid_h"][0]
        P_e_nom = (1 - share_peak) * in_meteo_data_forsizing["Q_h"] / in_meteo_data_forsizing["COP_h"]
        HC_1 = P_e_nom[0] * estim_COP_vector(in_meteo_data["T_source_h"], T_fluid_peak, in_meteo_data["DOF"],
                                             in_params, mode="Heating")
        HC_2 = HC_1 * (1 - in_params["coeff_HC"] * (in_meteo_data["T_fluid_h"] - T_fluid_peak) / T_fluid_peak)
        in_meteo_data["HC"] = HC_2
        if in_params["Consider_cooling"]:
            CC_1 = P_e_nom[0] * estim_COP_vector(in_meteo_data["T_source_c"], T_fluid_peak, in_meteo_data["DOF"],
                                                 in_params, mode="Cooling")
            CC_2 = CC_1 * (1 - in_params["coeff_HC"] * (in_meteo_data["T_fluid_c"] - T_fluid_peak) / T_fluid_peak)
            in_meteo_data["CC"] = CC_2
        else:
            in_meteo_data["CC"] = np.nan
    else:
        T_fluid_peak = in_meteo_data_forsizing["T_fluid_c"][0]
        P_e_nom = (1 - share_peak) * in_meteo_data_forsizing["Q_c"] / in_meteo_data_forsizing["COP_c"]
        CC_1 = P_e_nom[0] * estim_COP_vector(in_meteo_data["T_source_c"], T_fluid_peak, in_meteo_data["DOF"],
                                             in_params, mode="Cooling")
        CC_2 = CC_1 * (1 - in_params["coeff_HC"] * (in_meteo_data["T_fluid_c"] - T_fluid_peak) / T_fluid_peak)
        in_meteo_data["CC"] = CC_2
        HC_1 = P_e_nom[0] * estim_COP_vector(in_meteo_data["T_source_h"], T_fluid_peak, in_meteo_data["DOF"],
                                             in_params, mode="Heating")
        HC_2 = HC_1 * (1 - in_params["coeff_HC"] * (in_meteo_data["T_fluid_h"] - T_fluid_peak) / T_fluid_peak)
        in_meteo_data["HC"] = HC_2


def coeffs_T_fluid(comp_params, in_params):
    """
    Depending on the sizing temperature and the water regime of radiators, this function calculates the coefficient of
    the weather compensation.

    :param comp_params: all the parameters having respect with the sizing of equipment, and points of bivalence for instance
    :param in_params: input parameters of the simulated system
    :return: the values of the coefficients of the weather compensation
    """
    global TemperatureValues
    global TemperatureValuesCooling

    if not "T_fluid_h" in in_params:
        in_params["T_fluid_h"] = TemperatureValues[in_params["Emitters"]]
        in_params["T_fluid_c"] = TemperatureValuesCooling[in_params["Emitters"]]

    res = {}
    for ind in ["h", "c"]:
        T_base = comp_params["T_base_" + ind]
        Temp_fluid = in_params["T_fluid_" + ind]
        offset = 10 if ind == "h" else -5

        T_target = in_params["T_target_heating"] if ind == "h" else in_params["T_target_cooling"]
        if in_params["System"] == "A/A HP":  # PAC AIR/AIR
            res["a_" + ind] = (Temp_fluid - (T_target + offset)) / (
                    T_base - T_target)
            res["b_" + ind] = Temp_fluid - res["a_" + ind] * T_base
        if ((in_params["System"] in ["A/W HP", "W/W HP"]) | (
                (in_params["System"] == "chaudiere") & (
                in_params["Technology"] == "condensation"))):  # PAC AIR/EAU
            res["a_" + ind] = (Temp_fluid - (T_target + offset)) / (
                    T_base - T_target)
            res["b_" + ind] = Temp_fluid - res["a_" + ind] * T_base
        if not in_params["Weather_compensation"]:
            res["b_" + ind] = res["a_" + ind] * T_base + res["b_" + ind]
            res["a_" + ind] = 0

    sol_res = {key: round(res[key], 3) for key in res}
    return sol_res;


def estim_COP(T_source, T_fluid, DOF, type="A/W HP"):
    """
    The COP is dependant of both the sink and source temperature. This function estimates the COP (or EER) at one
    point (time step) taking into account the temperatures of fluid (sink and source) and the degradation of
    performances due to defrost

    :param T_source: Source temperature
    :param T_fluid: Sink temperature
    :param DOF: Degradation due to defrost (in %)
    :param type: The type of heat pump. The coefficients of the COP curves are different for A/A, A/W and W/W HPs
    :return: value of the COP
    """

    global new_nominal_COP_curve_Coefficients
    Delta_T = T_fluid - T_source
    a2 = new_nominal_COP_curve_Coefficients[type]["a2"]
    b2 = new_nominal_COP_curve_Coefficients[type]["b2"]
    c2 = new_nominal_COP_curve_Coefficients[type]["c2"]
    a1 = new_nominal_COP_curve_Coefficients[type]["a1"]
    b1 = new_nominal_COP_curve_Coefficients[type]["b1"]
    c1 = new_nominal_COP_curve_Coefficients[type]["c1"]
    d1 = new_nominal_COP_curve_Coefficients[type]["d1"]

    T_fluid_bis = 55 if T_fluid > 55 else 30 if T_fluid < 30 else T_fluid  # we adjust the fluid temperature to avoid extra high or extra low COP
    if T_source <= -3:
        res = a2 + b2 * Delta_T + c2 * Delta_T ** 2
    elif T_source >= 6:
        res = a1 + b1 * Delta_T + c1 * Delta_T ** 2 + d1 * T_fluid_bis
    else:
        res = (T_source - 6) / (-9) * (a2 + b2 * Delta_T + c2 * Delta_T ** 2) + (T_source + 3) / 9 * (
                a1 + b1 * Delta_T + c1 * Delta_T ** 2 + d1 * T_fluid_bis)

    res = res * (1 + DOF)
    return round(res, 3)


def estim_COP_vector(T_source, T_fluid, DOF, in_params, mode="Heating"):
    """
    The COP is dependant of both the sink and source temperature. This function estimates the COP (or EER) at all the
    time steps of the input time serie thanks to a vectorial structure. It takes into account the temperatures of
    fluids and the degradation of performances due to defrost

    :param T_source: Source temperature (vector)
    :param T_fluid: Sink temperature (vector)
    :param DOF: Degradation due to defrost (in %) (vector)
    :param in_params: input parameters of the simulated system
    :param mode: Heating or cooling. if cooling, the COP curve is the COP of heating - 1
    :return: value of the COP (vector)
    """
    global new_nominal_COP_curve_Coefficients

    type = in_params["System"]

    if isinstance(T_fluid, (int, float)):
        T_fluid2 = np.repeat(T_fluid, len(T_source), axis=0)
        T_fluid2 = pd.DataFrame(T_fluid2, columns=["T_fluid"])
        T_fluid2["date"] = T_source.index
        T_fluid2.set_index(["date"], inplace=True)
        T_fluid = T_fluid2["T_fluid"]

    Delta_T = T_fluid - T_source
    a2 = new_nominal_COP_curve_Coefficients[type]["a2"]
    b2 = new_nominal_COP_curve_Coefficients[type]["b2"]
    c2 = new_nominal_COP_curve_Coefficients[type]["c2"]
    a1 = new_nominal_COP_curve_Coefficients[type]["a1"]
    b1 = new_nominal_COP_curve_Coefficients[type]["b1"]
    c1 = new_nominal_COP_curve_Coefficients[type]["c1"]
    d1 = new_nominal_COP_curve_Coefficients[type]["d1"]
    res = pd.Series(index=T_source.index, data=0.)

    if mode == "Cooling":
        a2 = a2 - 1
        a1 = a1 - 1

    mask_Tfluid_low = T_fluid <= 30
    mask_Tfluid_high = T_fluid > 55
    T_fluid_bis = T_fluid.copy()
    T_fluid_bis.loc[mask_Tfluid_low] = 30
    T_fluid_bis.loc[mask_Tfluid_high] = 55

    mask_low = T_source <= -3
    mask_between = T_source.between(-3, 6)
    mask_high = T_source > 6

    res.loc[mask_low] = a2 + b2 * Delta_T.loc[mask_low] + c2 * Delta_T.loc[mask_low] ** 2
    res.loc[mask_between] = a1 + b1 * Delta_T.loc[mask_between] + c1 * Delta_T.loc[mask_between] ** 2 + d1 * \
                            T_fluid_bis.loc[mask_between]
    res.loc[mask_between] = (T_source.loc[mask_between] - 6) / (-9) * (
            a2 + b2 * Delta_T.loc[mask_between] + c2 * Delta_T.loc[mask_between] ** 2) + (
                                    T_source.loc[mask_between] + 3) / 9 * (
                                    a1 + b1 * Delta_T.loc[mask_between] + c1 * Delta_T.loc[mask_between] ** 2 + d1 *
                                    T_fluid_bis.loc[mask_between])
    res.loc[mask_high] = a1 + b1 * Delta_T.loc[mask_high] + c1 * Delta_T.loc[mask_high] ** 2 + d1 * T_fluid_bis.loc[
        mask_high]

    res = res * (1 + DOF)

    return np.round(res, 3)

def compute_T_base(meteo_data, n_days=5):
    """
    The base temperature is the lowest temperature for which the heating system is designed to fulfill the heating needs.
    This function computes the base temperature for a given climate using a definition based on NF EN 12831,(the lowest
    temperature recorded at least five days during the year).
    The best would be to estimate it from long term data to get the extremes. If not, this function can estimate a value

    :param in_meteo_data: weather data time serie, including relative humidity and temperature
    :param n_days: number of days for which the base temperature is reached
    :return: Base temperatures for heating and cooling
    """

    temperature = meteo_data[["temp", "day_of_year", "year"]]

    min_daily_temperature = temperature.groupby(['day_of_year', 'year']).temp.min()
    max_daily_temperature = temperature.groupby(['day_of_year', 'year']).temp.max()

    T_base_h = round(np.quantile(min_daily_temperature, q=n_days / 365), 3)
    T_base_c = round(np.quantile(max_daily_temperature, q=(365 - n_days) / 365), 3)

    return T_base_h, T_base_c


def estim_SCOP(meteo_data, in_params, year=None, consider_PL=True, oversizing_factor=1,
               force_bivalent=False, adjust_T_fluid=True, consider_defrost_equation=True, sized_for="Heating"):
    """
    This is the master function, which will to all the calculation of the performance indicators over the year.
    It encapsulates all the modeling on base temperatures, defrosting, weather compensation etc.

    :param in_meteo_data: weather data time serie, including relative humidity and temperature
    :param in_params: number of days for which the base temperature is reached
    :param year: year of the simulation, if meteodata has to be extracted as a subset of long term weather data
    :param consider_PL: Do you want to consider the part-load modeling ? (True/False)
    :param oversizing_factor: The oversizing of the HP. If 1, it is well sized, if over 1, oversized by the factor
    :param force_bivalent: Do you want to force the bivalent mode (i.e. even if oversized, the backup still operates) ? (True/False)
    :param adjust_T_fluid: Do you want to adjust the fluid temperature after retrofit ? (True/False)
    :param consider_defrost_equation: Do you want to consider the defrost modeling ? (True/False)
    :param sized_for: Heating or Cooling, will choose which base temperature is used to size the equipment
    :return:A dictionnary with the COP curves, the seasonal performance indicators and the peak estimations
    """
    global new_nominal_COP_curve_Coefficients

    # Share_losses = 0.8
    year = str(year) if not (year is None) else year
    T_target_h = in_params["T_target_heating"]
    T_start_h = in_params["T_start_heating"]
    T_target_c = in_params["T_target_cooling"]
    T_start_c = in_params["T_start_cooling"]

    comp_params = {}

    # get_info_point(in_params, comp_params, "T_base")
    if "T_base_h" in in_params:
        comp_params["T_base_h"] = round(in_params["T_base_h"], 3) if in_params["T_base_h"] < meteo_data["temp"].min() else \
        round(meteo_data["temp"].min(),3)
        comp_params["T_base_c"] = round(in_params["T_base_c"], 3) if in_params["T_base_c"] > meteo_data["temp"].max() else \
        round(meteo_data["temp"].max(), 3)
    else:
        comp_params["T_base_h"], comp_params["T_base_c"] = compute_T_base(meteo_data)


    meteo_data["day"] = meteo_data.index.day
    meteo_data["day_of_year"] = meteo_data.index.day_of_year
    meteo_data["month"] = meteo_data.index.month
    meteo_data["year"] = meteo_data.index.year

    get_temperature_ground(meteo_data, depth=1.5)
    comp_params["gtemp_min"] = np.min(meteo_data["gtemp"])  # compute_gTemp_from_Temp(meteo_data)

    # get_info_point(in_params, comp_params, "T_base")
    if "T_peak_h" in in_params:
        comp_params["T_peak_h"] = in_params["T_peak_h"] if in_params["T_peak_h"] < meteo_data["temp"].min() else \
        meteo_data["temp"].min()
        comp_params["T_peak_c"] = in_params["T_peak_c"] if in_params["T_peak_c"] > meteo_data["temp"].max() else \
        meteo_data["temp"].max()

    meteo_data = meteo_data if year is None else meteo_data.loc[year, :]
    get_heating_period_metdata(meteo_data, T_start_h)
    in_params["T_min"] = round(min(min(meteo_data["temp"]), comp_params["T_peak_h"]), 3)
    in_params["T_max"] = round(max(max(meteo_data["temp"]), comp_params["T_peak_c"]), 3)

    # SI IL Y A UNE REGULATION DE LA TEMPERATURE DE L'EAU, ON AJUSTE
    coeffs_heating = coeffs_T_fluid(comp_params,
                                    in_params=in_params)  ## calcul de a et b
    comp_params = {**comp_params, **coeffs_heating}



    if "T_peak_h" in in_params:
        meteo_data_peak_heating = pd.DataFrame(
            data={"temp": comp_params["T_peak_h"], "rh": 100, "gtemp": comp_params["gtemp_min"]},
            index=["Peak_h"])
        meteo_data_peak_heating.index.name = 'date'
        meteo_data = pd.concat([meteo_data, meteo_data_peak_heating], axis=0)
        if in_params["Consider_cooling"]:
            meteo_data_peak_cooling = pd.DataFrame(
                data={"temp": comp_params["T_peak_c"], "rh": 0, "gtemp": comp_params["gtemp_min"]},
                index=["Peak_c"])
            meteo_data_peak_cooling.index.name = 'date'
            meteo_data = pd.concat([meteo_data, meteo_data_peak_cooling], axis=0)

    index_under = meteo_data['temp'] < comp_params["T_base_h"]

    T_fluid = comp_params["a_h"] * meteo_data['temp'] + comp_params["b_h"]
    T_fluid.loc[index_under] = comp_params["a_h"] * comp_params["T_base_h"] + comp_params["b_h"]
    meteo_data["T_fluid_h"] = T_fluid


    meteo_data['T_source_h'] = meteo_data['gtemp'] if in_params["System"] == "W/W HP" else meteo_data['temp']

    if in_params["Consider_cooling"]:
        meteo_data["T_fluid_c"] = meteo_data['temp']

        index_over = meteo_data['temp'] > comp_params["T_base_c"]

        T_source = comp_params["a_c"] * meteo_data['temp'] + comp_params["b_c"]
        T_source.loc[index_over] = comp_params["a_c"] * comp_params["T_base_c"] + comp_params["b_c"]
        meteo_data["T_source_c"] = T_source

    estimate_impact_defrost(meteo_data, consider_defrost_equation)

    meteo_data["Q_c"] = (meteo_data["temp"] > T_target_c) * (
            meteo_data['temp'] - T_target_c)  # WS for Well-sized
    meteo_data["Q_h"] = (meteo_data["temp"] < T_target_h) * (
            T_target_h - meteo_data['temp'])  # WS for Well-sized

    meteo_data["COP_h"] = estim_COP_vector(meteo_data['T_source_h'],
                                           meteo_data['T_fluid_h'],
                                           meteo_data["DOF"],
                                           in_params)
    if in_params["Consider_cooling"]:
        meteo_data["COP_c"] = estim_COP_vector(meteo_data['T_source_c'],
                                               meteo_data['T_fluid_c'],
                                               0,
                                               in_params,
                                               "Cooling")
    else:
        meteo_data["COP_c"] = np.nan

    if sized_for == "Heating":
        meteo_data_peak_heating = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_h")[0], :]
        estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_peak_heating, sized_for=sized_for)
    else:
        meteo_data_peak_cooling = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_c")[0], :]
        estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_peak_cooling, sized_for=sized_for)

    meteo_data['Q_h_OS'] = meteo_data['Q_h'] / oversizing_factor
    meteo_data['Q_c_OS'] = meteo_data['Q_c'] / oversizing_factor if in_params["Consider_cooling"] else np.nan

    comp_params["T_off"] = in_params["Temperature_limit"]
    meteo_data["Off"] = meteo_data["temp"] < in_params["Temperature_limit"]

    if sized_for == "Heating":
        if in_params["Mode"] == "Bivalent parallel": ## If in bivalent parallel, we have to loop to find which sizing will guarantee the right coverage by the backup
            meteo_data_nopeak = meteo_data.copy().iloc[np.where(~(meteo_data.index.isin(["Peak_h", "Peak_c"])))[0], :]
            list_share_peak = np.arange(50, 1000, 25, dtype=int) / 1000
            meteo_data_peak_heating = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_h")[0], :]
            meteo_data_peak_cooling = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_c")[0], :]

            list_RE = []
            list_RP = []
            list_meteo = []
            for share_peak in list_share_peak:
                estimate_heating_cooling_capacity(meteo_data_nopeak, in_params, meteo_data_peak_heating,
                                                  sized_for=sized_for,
                                                  share_peak=share_peak)

                if in_params['Technology'] == 'Inverter':
                    tmp_meteo_data = new_estimate_energies_Inverter(in_params,
                                                                    meteo_data_nopeak,
                                                                    comp_params,
                                                                    consider_PL)
                elif in_params['Technology'] == 'Bi-compressor':
                    tmp_meteo_data = new_estimate_energies_BiCompressor(in_params,
                                                                        meteo_data_nopeak,
                                                                        comp_params,
                                                                        consider_PL)
                else:
                    tmp_meteo_data = new_estimate_energies_OnOff(in_params,
                                                                 meteo_data_nopeak, comp_params,
                                                                 consider_PL)

                RE = tmp_meteo_data["P_app_h"].sum() / (
                        tmp_meteo_data["P_calo_h"] + tmp_meteo_data["P_app_h"]).sum()
                RP = tmp_meteo_data["P_app_h"] / (
                        tmp_meteo_data["P_calo_h"] + tmp_meteo_data["P_app_h"])

                list_meteo.append(tmp_meteo_data.copy())
                list_RE.append(RE)
                list_RP.append(RP)
            which_match_share_peak = np.where(list_RE == min(list_RE,
                                                             key=lambda x: abs(
                                                                 x - in_params["Share_energy"])))[
                0].item() # Here we select the sizing with maps the coverage by the backup and after we rerun the estimations

            in_params["Share_peak"] = list_share_peak[which_match_share_peak]
            estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_peak_heating, sized_for=sized_for,
                                              share_peak=in_params["Share_peak"])
        elif in_params["Mode"] == "Bivalent alternative":
            meteo_data_forsizing = compute_Toff_bivalent(meteo_data, in_params["Share_energy"])
            meteo_data_forsizing["rh"] = 100
            estimate_impact_defrost(meteo_data_forsizing, consider_defrost_equation)
            meteo_data_forsizing["COP_h"] = estim_COP_vector(meteo_data_forsizing['T_source_h'],
                                                             meteo_data_forsizing['T_fluid_h'],
                                                             meteo_data_forsizing["DOF"],
                                                             in_params)

            estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_forsizing, sized_for="Heating")

            off = meteo_data["temp"] < meteo_data_forsizing["temp"][0]
            meteo_data["Off"] = off

            estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_forsizing, sized_for="Heating")
        else:
            meteo_data_peak_heating = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_h")[0], :]
            estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_peak_heating, sized_for=sized_for,
                                              share_peak=in_params["Share_peak"])
    else:
        meteo_data_peak_cooling = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_c")[0], :]
        estimate_heating_cooling_capacity(meteo_data, in_params, meteo_data_peak_cooling, sized_for=sized_for)
    meteo_data_peak_heating_original = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_h")[0], :]
    meteo_data_peak_cooling = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_c")[0], :]

    if oversizing_factor > 1:
        if adjust_T_fluid:
            comp_params["a_h"] = comp_params["a_h"] / oversizing_factor
            Temp_emitters = comp_params["a_h"] * (comp_params["T_base_h"] - in_params['T_target_heating']) + \
                            in_params[
                                'T_target_heating'] + 10
            comp_params["b_h"] = Temp_emitters - comp_params["a_h"] * comp_params["T_base_h"]

            index_under = meteo_data['temp'] < comp_params["T_base_h"]

            T_fluid = comp_params["a_h"] * meteo_data['temp'] + comp_params["b_h"]
            T_fluid.loc[index_under] = comp_params["a_h"] * comp_params["T_base_h"] + comp_params["b_h"]
            meteo_data["T_fluid_h"] = T_fluid
            meteo_data["COP_h"] = estim_COP_vector(meteo_data['T_source_h'],
                                                   meteo_data['T_fluid_h'],
                                                   meteo_data['DOF'],
                                                   in_params)
            if (in_params["Mode"] == "Bivalent alternative") and (not force_bivalent):
                off = meteo_data["Off"].copy()
                off[(meteo_data["HC"] - meteo_data["Q_h_OS"]) < 0] = True
                off[(meteo_data["HC"] - meteo_data["Q_h_OS"]) >= 0] = False
                meteo_data["Off"] = off

    if in_params['Technology'] == 'Inverter':
        meteo_data = new_estimate_energies_Inverter(in_params,
                                                    meteo_data,
                                                    comp_params,
                                                    consider_PL,
                                                    oversizing_factor,
                                                    force_bivalent)

    elif in_params['Technology'] == 'Bi-compressor':
        meteo_data = new_estimate_energies_BiCompressor(in_params,
                                                        meteo_data,
                                                        comp_params,
                                                        consider_PL,
                                                        oversizing_factor,
                                                        force_bivalent)
    else:
        meteo_data = new_estimate_energies_OnOff(in_params,
                                                 meteo_data,
                                                 comp_params,
                                                 consider_PL,
                                                 oversizing_factor,
                                                 force_bivalent)

    meteo_data_nopeak = meteo_data.copy().iloc[np.where(~(meteo_data.index.isin(["Peak_h", "Peak_c"])))[0], :]
    RE = meteo_data_nopeak["P_app_h"].sum() / (
            meteo_data_nopeak["P_calo_h"] + meteo_data_nopeak["P_app_h"]).sum()
    RP = (meteo_data["P_app_h"] / (
            meteo_data["P_calo_h"] + meteo_data["P_app_h"])).to_frame()
    RP.columns = ["RP"]

    meteo_data["COP_machine_h"] = meteo_data["P_calo_h"] / meteo_data["P_elec_h"]
    meteo_data["COP_tot_h"] = (meteo_data["P_calo_h"] + meteo_data["P_app_h"]) / (
            meteo_data["P_elec_h"] + meteo_data["P_app_h"]) if in_params["Mode"] in ["Monovalent",
                                                                                     "Spatial backup"] else meteo_data[
        "COP_machine_h"]
    meteo_data_peak_heating = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_h")[0], :]

    if in_params["Consider_cooling"]:
        meteo_data["EER_machine_c"] = meteo_data["P_calo_c"] / meteo_data["P_elec_c"]
        meteo_data_peak_cooling = meteo_data.copy().iloc[np.where(meteo_data.index == "Peak_c")[0], :]

    meteo_data = meteo_data.copy().iloc[np.where(~(meteo_data.index.isin(["Peak_h", "Peak_c"])))[0], :]

    SCOP_machine = meteo_data["P_calo_h"].sum() / meteo_data["P_elec_h"].sum()
    SCOP_tot = (meteo_data["P_calo_h"].sum() + meteo_data["P_app_h"].sum()) / (
            meteo_data["P_elec_h"].sum() + meteo_data["P_app_h"].sum()) if in_params["Mode"] in ["Monovalent",
                                                                                                 "Spatial backup"] else SCOP_machine
    SEER_machine = meteo_data["P_calo_c"].sum() / meteo_data["P_elec_c"].sum() if (in_params[
        "Consider_cooling"]) and (meteo_data["P_calo_c"].sum() > 0) else np.nan

    APF_machine = (meteo_data["P_calo_h"].sum() + meteo_data["P_calo_c"].sum()) / (
            meteo_data["P_elec_h"].sum() + meteo_data["P_elec_c"].sum()) if in_params[
        "Consider_cooling"] else SCOP_machine
    APF_tot = ((meteo_data["P_calo_h"].sum() + meteo_data["P_app_h"].sum() + meteo_data["P_calo_c"].sum()) / (
            meteo_data["P_elec_h"].sum() + meteo_data["P_app_h"].sum() + meteo_data["P_elec_c"].sum()) if in_params[
                                                                                                              "Mode"] in [
                                                                                                              "Monovalent",
                                                                                                              "Spatial backup"] else APF_machine) if \
        in_params["Consider_cooling"] else SCOP_tot

    ## Getting the peak info
    Peak_COP_machine = meteo_data_peak_heating["COP_machine_h"].sum() if meteo_data_peak_heating[
                                                                             "P_elec_h"].sum() > 0 else np.nan
    Peak_COP_tot = meteo_data_peak_heating["COP_tot_h"].sum() if meteo_data_peak_heating[
                                                                     "P_elec_h"].sum() > 0 else np.nan
    Peak_share = (meteo_data_peak_heating["P_app_h"] / (
            meteo_data_peak_heating["P_calo_h"] + meteo_data_peak_heating["P_app_h"])).sum()

    Peak_EER_machine = meteo_data_peak_cooling["EER_machine_c"].sum() if in_params["Consider_cooling"] else np.nan

    if in_params["Mode"] == "Spatial backup":
        RE = RE / 2 + 0.5
        RP = RP / 2 + 0.5
        Peak_share = Peak_share / 2 + 0.5

    sample_meteo_for_costs = meteo_data_peak_heating.copy()
    sample_meteo_for_costs["temp"] = 10 if in_params["System"] == "A/A HP" else 7
    sample_meteo_for_costs['T_source_h'] = sample_meteo_for_costs['temp']  # W/W not implemented
    sample_meteo_for_costs["T_fluid_h"] = 20 if in_params["System"] == "A/A HP" else 35
    sample_meteo_for_costs["COP_h"] = estim_COP(sample_meteo_for_costs["T_source_h"][0],
                                                sample_meteo_for_costs["T_fluid_h"][0],0,
                                                type=in_params["System"])
    sample_meteo_for_costs["Q_h"] = (sample_meteo_for_costs["temp"] < T_target_h) * (
            T_target_h - sample_meteo_for_costs['temp'])
    if (in_params["Mode"] == "Bivalent alternative") and (sized_for == "Heating"):
        estimate_heating_cooling_capacity(sample_meteo_for_costs, in_params, meteo_data_forsizing, sized_for="Heating")
    else:
        estimate_heating_cooling_capacity(sample_meteo_for_costs, in_params, meteo_data_peak_heating_original,
                                          sized_for="Heating", share_peak=in_params["Share_peak"])
    comp_params["HC_nom"] = sample_meteo_for_costs["HC"][0]
    comp_params["Pelec_nom"] = sample_meteo_for_costs["HC"][0] / sample_meteo_for_costs["COP_h"][0]

    KPIs = pd.DataFrame(
        np.array([[SCOP_machine, SCOP_tot, SEER_machine, APF_machine, APF_tot],
                  [Peak_COP_machine, Peak_COP_tot, Peak_EER_machine, np.nan, np.nan]]),
        columns=["machine_h", "tot_h", "machine_c", "machine_APF", "tot_APF"],
        index=["SCOPs", "Peaks"])

    CDD = meteo_data["Q_c"].sum()
    HDD = meteo_data["Q_h"].sum()


    return {"KPIs": KPIs,
            "RP": RP, "RE": RE, "meteo_data": meteo_data, "Pelec_nom": comp_params["Pelec_nom"],
            "HC_nom": comp_params["HC_nom"],
            "Peak_share": Peak_share, "comp_params": comp_params,
            "CDD": CDD, "HDD": HDD}


def new_estimate_energies_Inverter(in_params, meteo_data, consider_PL,
                                   oversize=1, force_bivalent=False):
    """
    This function estimates, for the Inverter-driver HPs, the part load ratio and the corresponding boosting or
    degradation of COP. The calculation is based on an estimation of the calorific power produced, the electric power
    consumed and the power provided by the backup.

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param in_params: number of days for which the base temperature is reached
    :param consider_PL: Do you want to consider the part-load modeling ? (True/False)
    :param oversize: The oversizing of the HP. If 1, it is well sized, if over 1, oversized by the factor
    :param force_bivalent: Do you want to force the bivalent mode (i.e. even if oversized, the backup still operates) ? (True/False)
    :return: The meteo_data object, with the calorific power procided, electric power consumed, and power of the backup.
    """
    global Inverter_partload_Coefficients
    global OnOff_partload_Coefficients

    Inverter_PL_params = Inverter_partload_Coefficients[in_params["Model_Inverter"]]
    OnOff_PL_params = OnOff_partload_Coefficients[in_params["Model_Inverter"]]

    if consider_PL:
        if in_params["Model_Inverter"] == "v1":
            PLF_biv = Inverter_PL_params["PLF_biv"]
            a1 = 0
            a2 = (PLF_biv - 1) / (Inverter_PL_params["PLR_min"] - 1)
            a3 = 1 - a2
        elif in_params["Model_Inverter"] == "v2":
            a1 = Inverter_PL_params["a1"]
            a2 = Inverter_PL_params["a2"]
            a3 = Inverter_PL_params["a3"]
    else:
        a1 = 0
        a2 = 0
        a3 = 1

    if consider_PL:
        if in_params["Model_OnOff"] in ["v1", "v2"]:
            Cd = OnOff_PL_params["Cd"]
            Cc = OnOff_PL_params["Cc"]
            Ce = 0
        elif in_params["Model_OnOff"] == "v3":
            Ce = 0.25
            Cd = 0
            Cc = 1
    else:
        Ce = 0
        Cd = 0
        Cc = 1


    ### We loop for heating (h) and cooling (c)
    for ind in ["h", "c"]:
        meteo_data['Q_' + ind + '_OS'] = meteo_data['Q_' + ind] / oversize

        oversize = 1 if not force_bivalent else oversize

        min_PLR_CR = 1 / oversize

        C = meteo_data['HC'] if (ind == "h") else meteo_data['CC']
        meteo_data["PLR_CR_" + ind] = np.minimum(min_PLR_CR, meteo_data['Q_' + ind + '_OS'] / C)

        meteo_data["PLF_" + ind] = a1 * meteo_data["PLR_CR_" + ind] ** 2 + a2 * meteo_data[
            "PLR_CR_" + ind] + a3

        PLF_min = a1 * Inverter_PL_params["PLR_min"] ** 2 + a2 * Inverter_PL_params["PLR_min"] + a3
        meteo_data["PLF_" + ind] = np.minimum(meteo_data["PLF_" + ind], PLF_min)

        mask_off = meteo_data['Off']
        meteo_data.loc[mask_off, "PLR_CR_" + ind] = np.nan
        mask_high = meteo_data["PLR_CR_" + ind] == 1/ oversize
        mask_between = (meteo_data["PLR_CR_" + ind].between(Inverter_PL_params["PLR_min"], 1, inclusive='left')) * (meteo_data["PLR_CR_" + ind] < 1 / oversize)
        mask_low = (meteo_data["PLR_CR_" + ind] < Inverter_PL_params["PLR_min"]) * (meteo_data["PLR_CR_" + ind] < 1 / oversize)

        PLR_ma = meteo_data['Q_' + ind + '_OS'] / (C * Inverter_PL_params["PLR_min"])
        PLR_ma[mask_high] = 1/oversize/Inverter_PL_params["PLR_min"] if oversize>1 else 1
        PLR_ma[mask_between] = 1

        meteo_data["PLR_ma_" + ind] = PLR_ma
        meteo_data["Dp_" + ind] = 1 / (1 + Cd * (1 - meteo_data["PLR_ma_" + ind]) / (
                1 - Cd * (1 - meteo_data["PLR_ma_" + ind])) + (1 - Cc) *
                                       (1 - meteo_data["PLR_ma_" + ind]) / meteo_data[
                                           "PLR_ma_" + ind]) * (1 - Ce * (1 - meteo_data["PLR_ma_" + ind]))

        P_calo = pd.Series(index=meteo_data.index, data=0.)
        P_elec = pd.Series(index=meteo_data.index, data=0.)
        P_app = pd.Series(index=meteo_data.index, data=0.)
        # P_calo : besoin calorifique du bâtiment, dépend de la diff de temp entre T_target et T_ext
        P_calo.loc[mask_off] = 0
        P_calo.loc[mask_high] = C.loc[mask_high] / oversize
        P_calo.loc[mask_between] = meteo_data['Q_' + ind + '_OS'].loc[mask_between]
        P_calo.loc[mask_low] = meteo_data['Q_' + ind + '_OS'].loc[mask_low]
        meteo_data["P_calo_" + ind] = P_calo

        P_elec.loc[mask_off] = 0
        P_elec.loc[mask_high] = (meteo_data["P_calo_" + ind] / meteo_data['COP_' + ind]).loc[mask_high]
        P_elec.loc[mask_between] = \
            (meteo_data["P_calo_" + ind] / (meteo_data['COP_' + ind] * meteo_data['PLF_' + ind])).loc[mask_between]
        P_elec.loc[mask_low] = \
            (meteo_data["P_calo_" + ind] / (meteo_data['COP_' + ind] * PLF_min * meteo_data['Dp_' + ind])).loc[mask_low]
        meteo_data["P_elec_" + ind] = P_elec

        P_app.loc[mask_off] = meteo_data['Q_' + ind + '_OS'].loc[mask_off]
        P_app.loc[mask_high] = (meteo_data['Q_' + ind + '_OS'] - meteo_data['P_calo_' + ind]).loc[
            mask_high]
        P_app.loc[mask_between] = (meteo_data['Q_' + ind + '_OS'] - meteo_data['P_calo_' + ind]).loc[
            mask_between]
        P_app.loc[mask_low] = (meteo_data['Q_' + ind + '_OS'] - meteo_data['P_calo_' + ind]).loc[
            mask_low]
        meteo_data["P_app_" + ind] = P_app

    return meteo_data


def new_estimate_energies_BiCompressor(in_params, meteo_data, consider_PL, oversize=1,
                                       force_bivalent=False):
    """
    This function estimates, for the Bi-compressor HPs, the part load ratio and the corresponding boosting or
    degradation of COP. The calculation is based on an estimation of the calorific power produced, the electric power
    consumed and the power provided by the backup.

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param in_params: number of days for which the base temperature is reached
    :param consider_PL: Do you want to consider the part-load modeling ? (True/False)
    :param oversize: The oversizing of the HP. If 1, it is well sized, if over 1, oversized by the factor
    :param force_bivalent: Do you want to force the bivalent mode (i.e. even if oversized, the backup still operates) ? (True/False)
    :return: The meteo_data object, with the calorific power procided, electric power consumed, and power of the backup.
    """
    global OnOff_partload_Coefficients
    OnOff_PL_params = OnOff_partload_Coefficients[in_params["Model_OnOff"]]
    if consider_PL:
        if in_params["Model_OnOff"] in ["v1", "v2"]:
            Cd = OnOff_PL_params["Cd"]
            Cc = OnOff_PL_params["Cc"]
            Ce = 0
        elif in_params["Model_OnOff"] == "v3":
            Ce = 0.25
            Cd = 0
            Cc = 1
    else:
        Ce = 0
        Cd = 0
        Cc = 1

    ### We loop for heating (h) and cooling (c)
    for ind in ["h", "c"]:
        meteo_data['Q_' + ind + '_OS'] = meteo_data['Q_' + ind] / oversize

        oversize = 1 if not force_bivalent else oversize

        min_PLR_ma = 1 / oversize

        C = meteo_data['HC'] if (ind == "h") else meteo_data['CC']

        global BiCompressor_partload_Coefficients
        Bicompressor_PL_params = BiCompressor_partload_Coefficients[in_params["Model_BiCompressor"]]

        C2 = Bicompressor_PL_params["PLR_min"] * C
        if ind == "h":
            meteo_data['HC2'] = C2
        else:
            meteo_data['CC2'] = C2

        meteo_data["PLR_ma1_" + ind] = np.minimum(min_PLR_ma, meteo_data["Q_" + ind + "_OS"] / C)
        meteo_data["Dp1_" + ind] = 1 / (
                1 + Cd * (1 - meteo_data["PLR_ma1_" + ind]) / (1 - Cd * (1 - meteo_data["PLR_ma1_" + ind])) + (
                1 - Cc) *
                (1 - meteo_data["PLR_ma1_" + ind]) / meteo_data["PLR_ma1_" + ind]) * (
                                           1 - Ce * (1 - meteo_data["PLR_ma1_" + ind]))

        mask_off = meteo_data['Off']
        meteo_data.loc[mask_off, "PLR_ma1_" + ind] = np.nan
        meteo_data.loc[mask_off, "PLR_ma2_" + ind] = np.nan
        mask_high = meteo_data["PLR_ma1_" + ind] == 1/oversize
        mask_low = (meteo_data["PLR_ma1_" + ind] >= Bicompressor_PL_params["PLR_min"])  * (meteo_data["PLR_ma1_" + ind] < 1 / oversize)

        PLR_ma2 = meteo_data['Q_' + ind + '_OS'] / C2
        PLR_ma2[mask_high] = 1 / oversize / Bicompressor_PL_params["PLR_min"]
        PLR_ma2[mask_low] = 1

        meteo_data["PLR_ma2_" + ind] = PLR_ma2
        meteo_data["Dp2_" + ind] = 1 / (
                1 + Cd * (1 - meteo_data["PLR_ma2_" + ind]) / (1 - Cd * (1 - meteo_data["PLR_ma2_" + ind])) + (
                1 - Cc) *
                (1 - meteo_data["PLR_ma2_" + ind]) / meteo_data["PLR_ma2_" + ind]) * (
                                           1 - Ce * (1 - meteo_data["PLR_ma2_" + ind]))

        mask_high = meteo_data["PLR_ma1_" + ind] == 1/ oversize
        mask_between = (meteo_data["PLR_ma1_" + ind] < 1) * (meteo_data["PLR_ma2_" + ind] == 1) * (
                    meteo_data["PLR_ma1_" + ind] < 1 / oversize)
        mask_low =( meteo_data["PLR_ma2_" + ind] < 1) * (meteo_data["PLR_ma1_" + ind] < 1 / oversize)

        P_calo = pd.Series(index=meteo_data.index, data=0.)
        P_elec = pd.Series(index=meteo_data.index, data=0.)
        P_app = pd.Series(index=meteo_data.index, data=0.)

        P_calo.loc[mask_off] = 0
        P_calo.loc[mask_high] = C.loc[mask_high] / oversize
        P_calo.loc[mask_between] = meteo_data["Q_" + ind + "_OS"].loc[mask_between]
        P_calo.loc[mask_low] = meteo_data["Q_" + ind + "_OS"].loc[mask_low]
        meteo_data["P_calo_" + ind] = P_calo

        P_elec.loc[mask_off] = 0
        P_elec.loc[mask_high] = (meteo_data["P_calo_" + ind] / meteo_data["COP_" + ind]).loc[mask_high]
        P_elec.loc[mask_between] = \
            (meteo_data["P_calo_" + ind] / (meteo_data["COP_" + ind] * meteo_data["Dp1_" + ind])).loc[mask_between]
        P_elec.loc[mask_low] = \
            (meteo_data["P_calo_" + ind] / (meteo_data["COP_" + ind] * meteo_data["Dp2_" + ind])).loc[mask_low]
        meteo_data["P_elec_" + ind] = P_elec

        P_app.loc[mask_off] = meteo_data['Q_' + ind + '_OS'].loc[mask_off]
        P_app.loc[mask_high] = (meteo_data['Q_' + ind + '_OS'] - meteo_data["P_calo_" + ind]).loc[
            mask_high]
        P_app.loc[mask_between] = (meteo_data['Q_' + ind + '_OS'] - meteo_data["P_calo_" + ind]).loc[mask_between]
        P_app.loc[mask_low] = (meteo_data['Q_' + ind + '_OS'] - meteo_data["P_calo_" + ind]).loc[mask_low]
        meteo_data["P_app_" + ind] = P_app

    return meteo_data


def new_estimate_energies_OnOff(in_params, meteo_data, consider_PL, oversize=1, force_bivalent=False):
    """
    This function estimates, for the single compressor HPs, the part load ratio and the corresponding boosting or
    degradation of COP. The calculation is based on an estimation of the calorific power produced, the electric power
    consumed and the power provided by the backup.

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param in_params: number of days for which the base temperature is reached
    :param consider_PL: Do you want to consider the part-load modeling ? (True/False)
    :param oversize: The oversizing of the HP. If 1, it is well sized, if over 1, oversized by the factor
    :param force_bivalent: Do you want to force the bivalent mode (i.e. even if oversized, the backup still operates) ? (True/False)
    :return: The meteo_data object, with the calorific power procided, electric power consumed, and power of the backup.
    """
    global OnOff_partload_Coefficients
    OnOff_PL_params = OnOff_partload_Coefficients[in_params["Model_OnOff"]]

    if consider_PL:
        if in_params["Model_OnOff"] in ["v1", "v2"]:
            Cd = OnOff_PL_params["Cd"]
            Cc = OnOff_PL_params["Cc"]
            Ce = 0
        elif in_params["Model_OnOff"] == "v3":
            Ce = 0.25
            Cd = 0
            Cc = 1
    else:
        Ce = 0
        Cd = 0
        Cc = 1


    ### We loop for heating (h) and cooling (c)
    for ind in ["h", "c"]:
        # comp_params["P_elec_nom"] = comp_params["Q_biv"] / comp_params["COP_biv"]
        meteo_data['Q_' + ind + '_OS'] = meteo_data['Q_' + ind] / oversize

        oversize = 1 if not force_bivalent else oversize

        min_PLR_ma = 1 / oversize

        C = meteo_data['HC'] if (ind == "h") else meteo_data['CC']

        meteo_data["PLR_ma_" + ind] = np.minimum(min_PLR_ma, meteo_data["Q_" + ind + "_OS"] / C)
        meteo_data["Dp_" + ind] = 1 / (
                1 + Cd * (1 - meteo_data["PLR_ma_" + ind]) / (1 - Cd * (1 - meteo_data["PLR_ma_" + ind])) + (
                1 - Cc) *
                (1 - meteo_data["PLR_ma_" + ind]) / meteo_data["PLR_ma_" + ind]) * (
                                          1 - Ce * (1 - meteo_data["PLR_ma_" + ind]))


        mask_off = meteo_data["Off"]
        meteo_data.loc[mask_off, "PLR_ma_" + ind] = np.nan
        mask_high = meteo_data["PLR_ma_" + ind] == 1 / oversize
        mask_low = meteo_data["PLR_ma_" + ind] < 1 / oversize

        P_calo = pd.Series(index=meteo_data.index, data=0.)
        P_elec = pd.Series(index=meteo_data.index, data=0.)
        P_app = pd.Series(index=meteo_data.index, data=0.)

        P_calo.loc[mask_off] = 0
        P_calo.loc[mask_high] = C.loc[mask_high] / oversize
        P_calo.loc[mask_low] = meteo_data["Q_" + ind + "_OS"].loc[mask_low]
        meteo_data["P_calo_" + ind] = P_calo

        P_elec.loc[mask_off] = 0
        P_elec.loc[mask_high] = (meteo_data["P_calo_" + ind] / meteo_data["COP_" + ind]).loc[mask_high]
        P_elec.loc[mask_low] = (meteo_data["P_calo_" + ind] / (meteo_data["COP_" + ind] * meteo_data["Dp_" + ind])).loc[
            mask_low]
        meteo_data["P_elec_" + ind] = P_elec

        P_app.loc[mask_off] = meteo_data['Q_' + ind + '_OS'].loc[mask_off]
        P_app.loc[mask_high] = (meteo_data['Q_' + ind + '_OS'] - meteo_data["P_calo_" + ind]).loc[mask_high]
        P_app.loc[mask_low] = (meteo_data['Q_' + ind + '_OS'] - meteo_data["P_calo_" + ind]).loc[mask_low]
        meteo_data["P_app_" + ind] = P_app

    return meteo_data

def get_heating_period_metdata(meteo_data, T_start):
    """
    This function split the weather data between the heating period and the time of the year where the heating is turned off.
    The cooling period is considered to be all year long.

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param T_start: temperature from which the people turn on their heating for the first time
    :return: A subset of the weather data
    """
    year = meteo_data.index.year[0]
    meteo_data.loc[:, "day"] = meteo_data.index.day
    meteo_data.loc[:, "month"] = meteo_data.index.month
    period_chauff = meteo_data.groupby(["month", "day"]).temp.mean().to_frame().rename(columns={"temp": "T_mean"})

    ##We calculate the rolling mean on 3 days to find the starting day
    rm_start = period_chauff.rolling(3).mean()
    ##We calculate the rolling mean on 7 days to find the stopping day
    rm_stop = period_chauff.rolling(7).mean()

    ##We split the data in 2 to get a continuous heating period
    second_half = rm_start.iloc[(round(len(rm_start) / 2)):len(rm_start)]
    first_half = rm_stop.iloc[0:(round((len(rm_stop) / 2)) + 1)]

    ##OWe identify when the heating starts --> Mean of temperatures below T_start for 3 days
    if not any(first_half.T_mean >= T_start):
        start_heating = second_half.index[-1]
    else:
        start_heating = second_half.loc[second_half.T_mean <= T_start - 2, :].index[0]
    ##We identify the moment when the heating stops --> Mean of temperatures over T_start for 7 days
    if not any(first_half.T_mean >= T_start):
        stop_heating = first_half.index[-1]
    else:
        stop_heating = first_half.loc[first_half.T_mean >= T_start, :].index[0]

    ### Different models exist, but calibrated for France (stadard). For changing conditions, we use the adjusted
    Periodes_chauffe = pd.DataFrame.from_dict(
        {"Method": ["Allyear", "Standard", "Adjusted"],
         "Start_month": [8, 10, start_heating[0]],
         "Start_day": [1, 1, start_heating[1]],
         "Stop_month": [7, 5, stop_heating[0]],
         "Stop_day": [31, 20, stop_heating[1]]}).set_index("Method")

    ## We use the adjusted method
    Periode_chauffe = Periodes_chauffe.loc["Adjusted",]
    start_date = pd.to_datetime(
        str(Periode_chauffe["Start_day"]) + "/" + str(Periode_chauffe["Start_month"]) + "/" + str(year), dayfirst=True)
    end_date = pd.to_datetime(
        str(Periode_chauffe["Stop_day"]) + "/" + str(Periode_chauffe["Stop_month"]) + "/" + str(year),
        dayfirst=True) + datetime.timedelta(days=1)

    meteo_data["heating_period"] = False
    meteo_data.loc[pd.to_datetime("1/1/" + str(year)): end_date, "heating_period"] = True
    meteo_data.loc[start_date:pd.to_datetime("1/1/" + str(year + 1)), "heating_period"] = True

    meteo_data["cooling_period"] = True


def compute_Toff_bivalent(meteo_data, share_energy):
    """
    In bivalent alternative, the backup has to be turned in below a certain temperature. In fact, the exact temperature
    is not easy to define as the defrost depends on relative humidity and meke the COP a non-bijective function.
    We thus approximate the switch temperature between the HP and the backup

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param share_energy: Share of energy which should be provided by the backup
    :return: A meteodata object with sizing peritting to reach the desired share
    """
    meteo_data_nopeak = meteo_data.copy().iloc[np.where(~(meteo_data.index.isin(["Peak_h", "Peak_c"])))[0], :]
    meteo_data_nopeak = meteo_data_nopeak.sort_values(by="temp", ascending=True)  # we order from colder to hotter days
    # meteo_data_nopeak = meteo_data_nopeak.sort_values(by="HC", ascending=True)  # we order from colder to hotter days
    meteo_data_nopeak["share_Q_h"] = meteo_data_nopeak["Q_h"].cumsum() / meteo_data_nopeak["Q_h"].sum()
    last_element = meteo_data_nopeak["share_Q_h"] >= share_energy
    meteo_data_forsizing = meteo_data_nopeak[last_element].iloc[[0]]

    return meteo_data_forsizing


def get_temperature_ground(meteo_data, depth):
    """
    For the specific case of ground source heat pumps, the temperature of the ground depends of the depth and the
    calorific capacity of the ground. The project does not aim at precisely estimating these performances over Europe
    and the ground temperature is approcimated very basicaly using the formula form Kusuda(https://www.nist.gov/publications/earth-temperature-and-thermal-diffusivity-selected-stations-united-states)

    :param meteo_data: weather data time serie, including relative humidity and temperature
    :param depth: Depth at which we want to estimate the ground temperature
    :return: meteo_data with a new column for the ground temperature
    """
    A = np.mean(meteo_data.temp)
    daily_mean = meteo_data.groupby("day_of_year").temp.mean()
    B = (daily_mean.max() + daily_mean.min()) / 2
    P = len(meteo_data)
    omega = (2 * np.pi / P)
    D = 0.054
    d = np.sqrt(2 * D / omega)
    t_0 = np.argmin(daily_mean) * 24
    phi = np.pi / 2 + omega * t_0
    ground_temp = A + B * math.exp(-depth / d) * np.sin(omega * pd.Series(range(0, P, 1)) - depth / d - phi)
    ground_temp = ground_temp.to_frame().set_index(meteo_data.index)
    meteo_data["gtemp"] = ground_temp


def extract_results(infos_df, Simulated_COP):
    """
    This function extracts the results from the estimate_SCOP function

    :param infos_df: previous dataframe containing details on the simulation (configuration)
    :param Simulated_COP: The results object of the simulation
    :return: complemented dataframe
    """
    if not Simulated_COP is None:
        infos_df["SCOP_machine"] = Simulated_COP["KPIs"].loc["SCOPs", "machine_h"]
        infos_df["SCOP_tot"] = Simulated_COP["KPIs"].loc["SCOPs", "tot_h"]
        infos_df["SEER_machine"] = Simulated_COP["KPIs"].loc["SCOPs", "machine_c"]
        infos_df["APF_machine"] = Simulated_COP["KPIs"].loc["SCOPs", "machine_APF"]
        infos_df["APF_tot"] = Simulated_COP["KPIs"].loc["SCOPs", "tot_APF"]

        infos_df["Energy_share"] = Simulated_COP["RE"]
        infos_df["Peak_COP_machine"] = Simulated_COP["KPIs"].loc["Peaks", "machine_h"]
        infos_df["Peak_COP_tot"] = Simulated_COP["KPIs"].loc["Peaks", "tot_h"]
        infos_df["Peak_EER_machine"] = Simulated_COP["KPIs"].loc["Peaks", "machine_c"]

        infos_df["Peak_share"] = Simulated_COP["Peak_share"]
        infos_df["HDD"] = Simulated_COP["HDD"] / 24
        infos_df["CDD"] = Simulated_COP["CDD"] / 24
    else:
        infos_df["SCOP_machine"] = np.nan
        infos_df["SCOP_tot"] = np.nan
        infos_df["SEER_machine"] = np.nan
        infos_df["APF_machine"] = np.nan
        infos_df["APF_tot"] = np.nan

        infos_df["Energy_share"] = np.nan
        infos_df["Peak_COP_machine"] = np.nan
        infos_df["Peak_COP_tot"] = np.nan
        infos_df["Peak_EER_machine"] = np.nan

        infos_df["Peak_share"] = np.nan
        infos_df["HDD"] = np.nan
        infos_df["CDD"] = np.nan
    return infos_df
