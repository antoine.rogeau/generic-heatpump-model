from pykml import parser
import xarray as xr
import geopandas as gpd
from tqdm import tqdm

from Generic_HeatPump_Modeling.functions import *


'''
 
ESTIMATING THE BASE TEMPERATURE AT ALL TMY POINTS (for sizing)

'''
### Defining pathes
Path_external_data = "PATH_TO_YOUR_DATA_FOLDER"
Data_path = Path_external_data + "Meteorological_data/"
Data_Folder = "Models/HeatPump_models/data/"
E_obs_data_path = Data_path + "ECMWF/input/E-OBS"

### Loading minimum and maximum daily temperatures
data_min = xr.open_dataset(E_obs_data_path + "/tn_ens_mean_0.1deg_reg_v27.0e.nc") # Download here: https://cds.climate.copernicus.eu/cdsapp#!/dataset/insitu-gridded-observations-europe?tab=overview
data_max = xr.open_dataset(E_obs_data_path + "/tx_ens_mean_0.1deg_reg_v28.0e.nc") # Download here: https://cds.climate.copernicus.eu/cdsapp#!/dataset/insitu-gridded-observations-europe?tab=overview

n_days_base = 5 # The definition of base temp: at least 5 observations a day
years=range(1992,2023) # Based on the 31 last years

#Looping over the years
list_base_xr = []
list_base_xr_cooling = []
for year in tqdm(years):
    ### For heating
    tmp_data_min = data_min.sel(time=slice(str(year)+'-01-01', str(year) + '-12-31'))
    tmp_base_xr = tmp_data_min.tn.quantile(n_days_base / 365, dim="time") # Finding the right quantile for the year for heating
    tmp_base_xr = tmp_base_xr.expand_dims(dim = {"time":[year]})
    list_base_xr.append(tmp_base_xr)

    ### Same for cooling
    tmp_data_max = data_max.sel(time=slice(str(year)+'-01-01', str(year) + '-12-31'))
    tmp_base_xr_cooling = tmp_data_max.tx.quantile((365-n_days_base) / 365, dim="time")
    tmp_base_xr_cooling = tmp_base_xr_cooling.expand_dims(dim = {"time":[year]})
    list_base_xr_cooling.append(tmp_base_xr_cooling)
Base_temp = xr.merge(list_base_xr)
Base_temp_cooling = xr.merge(list_base_xr_cooling)

### The minimum value at each grid point
T_base_xr = Base_temp.min(dim="time",skipna=True)
T_base_xr.rio.write_crs(4326,inplace=True)

### For cooling, it is the maximum value at each grid point
T_base_xr_cooling = Base_temp_cooling.max(dim="time",skipna=True)
T_base_xr_cooling.rio.write_crs(4326,inplace=True)

### Saving as ncdf files
T_base_xr = T_base_xr.drop_vars("quantile").rename({"tn":"T_base"})
T_base_xr.to_netcdf(Data_Folder + "T_base_Europe.nc")
T_base_xr_cooling = T_base_xr_cooling.drop_vars("quantile").rename({"tx":"T_base_cooling"})
T_base_xr_cooling.to_netcdf(Data_Folder + "T_base_cooling_Europe.nc")

### Getting coordinates of TMY points
loc_data_path = Data_path + "TMY/input"

with open(loc_data_path + '/Region6_Europe_EPW_Processing_locations.kml', 'r') as f:
  root = parser.parse(f).getroot()

namespace = {"kml":"http://earth.google.com/kml/2.1"}
pms = root.xpath(".//kml:Placemark[.//kml:Point]", namespaces=namespace)

list_data_point = []
k=0
for p in pms:
  coords = str(p.Point.coordinates)
  coords = coords.split(",")
  data_point = {"name":str(p.name.text),
                "lon":coords[0],
                "lat":coords[1]}
  list_data_point.append(pd.DataFrame(data_point,index=[k]))
  k+=1
all_data_point = pd.concat(list_data_point)
all_data_point = all_data_point.drop_duplicates()
all_data_point = all_data_point.iloc[1:]

all_data_point.to_csv(Data_path + "TMY/output/points_coordinates.csv",index=False)

#### Attaching a base temperature to each point
all_data_point = pd.read_csv(Data_path + "TMY/output/points_coordinates.csv",index=False)
gpd_point = gpd.GeoDataFrame(all_data_point,
                             geometry=gpd.points_from_xy(all_data_point.lon,all_data_point.lat),
                             crs="EPSG:4326")

T_base_xr = xr.open_dataset(Data_Folder + "T_base_Europe.nc")
T_base_xr_cooling = xr.open_dataset(Data_Folder + "T_base_cooling_Europe.nc")

T_base_xr = xr.merge([T_base_xr,T_base_xr_cooling])

a = T_base_xr.to_dataframe().reset_index()
a = a[~a.T_base.isna()]
a['LL'] = a['latitude'].astype(str) + a['longitude'].astype(str)

locs = a[['latitude', 'longitude']].value_counts().reset_index()
locs['LL'] = locs['latitude'].astype(str) + locs['longitude'].astype(str)

gdf1 = gpd.GeoDataFrame(
  locs,
  geometry=gpd.points_from_xy(locs.longitude, locs.latitude),
  crs="EPSG:4326"
)
gdf1 = gdf1.to_crs(crs=2154)

gpd_point = gpd_point.to_crs(crs=2154)

### Attaching the base temperature closer to each TMY points
dx = gpd.sjoin_nearest(gpd_point, gdf1, distance_col="distance",
  how='left')
a = a[a.LL.isin(dx['LL'])]

### We merge all the data and save
final_points = dx.merge(a)
final_points = final_points[["name","lon","lat","T_base","T_base_cooling"]]

final_points.to_csv(Data_Folder + "points_coordinates_with_Temps.csv",index=False)

