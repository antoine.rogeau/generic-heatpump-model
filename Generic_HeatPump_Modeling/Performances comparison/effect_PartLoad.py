from epw import epw
from tqdm import tqdm

from Generic_HeatPump_Modeling.functions import *

'''

LAUNCHING THE SIMULATIONS TO ESTIMATE THE EFFECT OF PART-LOAD CONSIDERATION ON ESTIMATED PERFORMANCES 
(Fig 4 of the article)

'''
### Defining pathes
Path_external_data = "PATH_TO_YOUR_DATA_FOLDER"
weather_data_path = Path_external_data + "Meteorological_data/TMY/input"

Data_Folder = "Models/HeatPump_models/data/"
Output_Folder = "Models/HeatPump_models/output/"

## Loading the info on peak temperatures (T_base)
T_base_points = pd.read_csv(Data_Folder + "points_coordinates_with_Temps.csv")

## We load the different heating systems considered
Systems = pd.read_csv(Data_Folder + "simple_chauffage.csv", sep=";")

## We chose which degradation model to integrate (see functions for detail)
Models = {"Model_OnOff": "v2", "Model_Inverter": "v1", "Model_BiCompressor": "v1"}
## Settings of heating and cooling start and target temperatures
Settings = {"T_start_heating": 15, "T_target_heating": 20,
                  "T_start_cooling": 30, "T_target_cooling": 25,
                  "coeff_HC":0.165} #coeff_HC: coefficient linking the heating capacity and the fluid temperatures. Source: https://link.springer.com/article/10.1007/s12273-012-0089-0

### Listing all the systems and compatibility between emitters, modes etc.
# For this study case, we focus on MT radiators
all_emitters_lists = {"A/A HP": ["Fan coil unit"],
                      "A/W HP": ["RadiatorMT"],
                      "W/W HP": ["RadiatorMT"]}
WC_list = [True, False]
all_mode_lists = {"A/A HP": ["Monovalent", "Spatial backup"],
                  "A/W HP": ["Monovalent", "Bivalent parallel", "Bivalent alternative"],
                  "W/W HP": ["Monovalent", "Bivalent parallel", "Bivalent alternative"]}

### The studycase is based on Paris weather data
a = epw()
country = "France"
file = "FRA_IF_Paris.MontSouris.071560"
a.read(weather_data_path + "/" + country + "/" + file + "_TMYx.2007-2021.epw")
meteodata = a.dataframe
meteodata["date"] = pd.to_datetime(dict(year=2018,
                                        month=meteodata.Month,
                                        day=meteodata.Day,
                                        hour=meteodata.Hour,
                                        minute=meteodata.Minute))
meteodata = meteodata[["date", "Dry Bulb Temperature","Relative Humidity"]].rename(columns={"Dry Bulb Temperature":"temp",
                                                                                                                "Relative Humidity":"rh"})
meteodata = meteodata.set_index("date")

#Splitting name of the file to get the name of the station
tmp_name = file.split("_")
tmp_name[2] = ".".join(tmp_name[2].split(".")[:-1])
tmp_name.reverse()
name_station = " ".join(tmp_name)

### Getting the peak temperature infos
T_base_h = T_base_points.loc[T_base_points.name==name_station,"T_base"].to_numpy()[0]
T_base_c = T_base_points.loc[T_base_points.name==name_station,"T_base_cooling"].to_numpy()[0]

### Keeping only the Air-to-water systems
Systems = Systems[Systems["System"] == "A/W HP"]

i=0
list_SCOP = []
list_COP = []
### Simulating both the cases where the part-load is considered or not
for consider_PL in [True,False]:
    for s in tqdm(Systems.index):
        System = Systems.loc[s, :]
        emitters_list = all_emitters_lists[System.System]
        mode_list = all_mode_lists[System.System]
        WC = "True" # We always consider weather compensation in this case
        for emitters in emitters_list: # There is only MT radiators in this sudy case
            for mode in mode_list:
                eff_backup = 1 if mode in ["Monovalent", "Spatial backup"] else 0.9 #Electric heating has an efficiency of 1, gas boiler of 0.9
                share_peak = 0.2 if mode in ["Monovalent", "Spatial backup"] else 0 # In monovalent, small backup for peak
                share_energy = 0.1 if mode in ["Bivalent parallel", "Bivalent alternative"] else np.nan # In bivalent, the backup covers 10% of the demand

                ### Setting the inputs
                configuration = {"Mode": mode,
                                 "Emitters": emitters,
                                 "Share_energy": share_energy,
                                 "Share_peak": share_peak,  # for monovalent systems
                                 "Weather_compensation": WC,
                                 "Consider_cooling": False,
                                 "T_base_h": T_base_h,
                                 "T_peak_h": T_base_h,
                                 "T_base_c": T_base_c,
                                 "T_peak_c": T_base_c}

                Simulation_PAC_input_parameter = {**Settings, **Models, **System.to_dict(),**configuration} # Merging the input

                ### Setting the information in the df used as output
                infos_df = pd.DataFrame(dict(Location=name_station,
                                             Consider_PL=consider_PL,
                                             System=Simulation_PAC_input_parameter["System"],
                                             Technology=Simulation_PAC_input_parameter["Technology"],
                                             Mode=Simulation_PAC_input_parameter["Mode"],
                                             Emitters=Simulation_PAC_input_parameter["Emitters"],
                                             Weather_compensation=Simulation_PAC_input_parameter["Weather_compensation"]), index=[i])

                ### Launching the simulation
                Simulated_COP = estim_SCOP(meteo_data=meteodata,
                                          in_params=Simulation_PAC_input_parameter,
                                          year=None,
                                          consider_PL=consider_PL,
                                          consider_defrost_equation=False )

                ### Storing results
                results = Simulated_COP.copy()
                results = extract_results(results, Simulated_COP)

                COP_df = results["meteo_data"][["COP_machine_h", "COP_tot_h"]]
                infos_df_rep = pd.DataFrame(np.repeat(infos_df.values, len(COP_df.index), axis=0),
                                            columns=infos_df.columns)
                infos_df_rep.reset_index(inplace=True)
                COP_data = pd.concat([COP_df, meteodata["temp"]], axis=1).reset_index()
                COP_data = pd.merge(COP_data, infos_df_rep, left_index=True, right_index=True)
                COP_data = COP_data.drop(columns=["date", "index"]).drop_duplicates()

                infos_df["SCOP"] = results["SCOP_machine"]

                list_COP.append(COP_data)
                list_SCOP.append(infos_df)
                i=i+1

### Save COP data
list_COP = pd.concat(list_COP)
list_COP.to_csv(Output_Folder + "compare_PL_COP_curves.csv")

### Save SCOP estimates
list_SCOP = pd.concat(list_SCOP)
list_SCOP.to_csv(Output_Folder + "compare_PL_SCOPs.csv")
