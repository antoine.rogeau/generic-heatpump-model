import xarray as xr
from rasterio.enums import Resampling
import numpy as np
import geopandas as gpd
import pandas as pd
from geocube.api.core import make_geocube

'''

AGGREGATION OF THE INDICATORS A DIFFERENT NUTS LEVELS

'''

### Defining pathes
Path_external_data = "PATH_TO_YOUR_DATA_FOLDER"

Data_Folder = "Models/HeatPump_models/data/"
Output_Folder = "Models/HeatPump_models/output/"

### Loading TMY estimations (just to have all configurations)
all_data_point = pd.read_csv(Output_Folder + "/TMY_simulations.csv")

### Loading the population dataset for weigthing
data_pop = xr.open_dataset(Path_external_data + "Maps/JRC/POPULATION/JRC_1K_POP_2018.tif") #download here: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat
data_pop = data_pop.sel(band=1)
data_pop = data_pop.rename({"band_data":"population"})

### Loading NUTS shapefile
data_NUTS = gpd.read_file(Path_external_data + "Maps/Eurostat/NUTS/NUTS_RG_01M_2021_4326.shp") # download here: https://ec.europa.eu/eurostat/fr/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts
data_NUTS.to_crs(2154, inplace=True)


### Defining a single configuration for debug runs
System = "A/W HP"
Technology = "Inverter"
Mode = "Monovalent"
Emitters = "RadiatorMT"
sized_for="Cooling"

### Loading the degree days dataset for the weighting and aggregation
data_degree_days = xr.open_dataset(Output_Folder + "Maps/new/Degree_days.nc")
data_degree_days.rio.write_crs("epsg:4326", inplace=True)

###Looping over all the systemps
for sized_for in ["Heating", "Cooling"]:
    data_demand = data_degree_days.HDD if sized_for == "Heating" else data_degree_days.CDD #Chosing between cooling and heating degree days
    data_demand = data_demand.where(data_demand > 0)
    data_peak = 20 - data_degree_days.T_peak_heating if sized_for == "Heating" else data_degree_days.T_peak_cooling - 25
    tmp_all_data_point = all_data_point[all_data_point["sized_for"] == sized_for] #Filtering
    tmp_all_data_point = tmp_all_data_point[~np.isnan(tmp_all_data_point["SCOP_machine"])]
    for System in tmp_all_data_point.System.unique():
        tmp_point_1=tmp_all_data_point[tmp_all_data_point.System==System] #Filtering
        for Technology in tmp_point_1.Technology.unique():
            tmp_point_2 = tmp_point_1[tmp_point_1.Technology == Technology] #Filtering
            for Mode in tmp_point_2.Mode.unique():
                tmp_point_3 = tmp_point_2[tmp_point_2.Mode == Mode] #Filtering
                for Emitters in tmp_point_3.Emitters.unique():
                    ### Loading the interpolated dataset generated in script XXX
                    data_SCOP = xr.open_dataset("Models/HeatPump_models/output/Maps/new/" +
                                        System.replace("/","") + "_" +
                                        Technology.replace("/","") + "_" +
                                        Mode + "_" +
                                        Emitters + "_sizedfor_" + sized_for + ".nc")

                    ### If Bivalent alternative, the Peak COP has no sense (full backup at peak)
                    if(Mode == "Bivalent alternative"):
                        data_SCOP = data_SCOP["SCOP_machine"].to_dataset() if sized_for == "Heating" else \
                        data_SCOP["SEER_machine"].to.dataset()
                    else:
                        data_SCOP = data_SCOP[["SCOP_machine","Peak_COP_machine"]] if sized_for == "Heating" else data_SCOP[["SEER_machine","Peak_EER_machine"]]
                    data_SCOP.rio.write_crs("epsg:4326", inplace=True)

                    data_SCOP_reproj = data_SCOP.rio.reproject_match(data_pop,resampling=Resampling.bilinear,nodata=np.nan)
                    data_demand_reproj = data_demand.rio.reproject_match(data_pop,resampling=Resampling.bilinear,nodata=np.nan)
                    data_peak_reproj = data_peak.rio.reproject_match(data_pop,resampling=Resampling.bilinear,nodata=np.nan)

                    final_xr = data_SCOP_reproj.merge(data_pop)
                    final_xr = final_xr.merge(data_demand_reproj.to_dataset(name="DD"))
                    final_xr = final_xr.merge(data_demand_reproj.to_dataset(name="Peak_Demand"))
                    final_xr = final_xr.rio.reproject("EPSG:2154")

                    #Looping over NUTS levels
                    list_NUTS = []
                    for LEVL in [0,1,2,3]:
                        data_NUTS_tmp = data_NUTS.copy().loc[data_NUTS.LEVL_CODE==LEVL]
                        data_NUTS_tmp["ID"] = np.linspace(0, len(data_NUTS_tmp.index) - 1, len(data_NUTS_tmp.index))

                        ### Create a xarray with the IDs of NUTS for further aggregation
                        out_grid = make_geocube(
                                vector_data=data_NUTS_tmp,
                                measurements=['ID'],
                                like=final_xr,  # ensure the data are on the same grid
                            )

                        dataset = xr.merge([out_grid, final_xr])
                        dataset = dataset.copy().to_dataframe()
                        dataset = dataset[~np.isnan(dataset.ID)]

                        IDs = data_NUTS_tmp[["NUTS_ID", "LEVL_CODE","ID"]]
                        dataset = dataset.merge(IDs)

                        data_wID = dataset.copy()

                        if sized_for == "Heating":
                            data_final = data_wID.loc[~np.isnan(data_wID.SCOP_machine),].copy()
                            data_final["pop_SCOP_DD"] = data_final.SCOP_machine * data_final.population * data_final.DD #Weigthing by population and degree days
                        else:
                            data_final = data_wID.loc[~np.isnan(data_wID.SEER_machine),].copy()
                            data_final["pop_SCOP_DD"] = data_final.SEER_machine * data_final.population * data_final.DD

                        data_final["pop_DD"] = data_final.population * data_final.DD
                        infos_scop =  data_final.groupby("NUTS_ID")["pop_SCOP_DD"].sum() / data_final.groupby("NUTS_ID")["pop_DD"].sum() #Dividing by the sum of population and degree days

                        final_info = infos_scop.rename("SCOP").reset_index() if sized_for == "Heating" else infos_scop.rename("SEER").reset_index()

                        ### At NUTS level 3, we also aggregate peak COP/EER
                        if (LEVL == 3) and (Mode != "Bivalent alternative"):
                            if sized_for == "Heating":
                                data_final2 = data_wID.loc[~np.isnan(data_wID.Peak_COP_machine),].copy()
                                data_final2["pop_Peak_COP"] = data_final2.Peak_COP_machine * data_final2.population * data_final2.Peak_Demand
                            else:
                                data_final2 = data_wID.loc[~np.isnan(data_wID.Peak_EER_machine),].copy()
                                data_final2["pop_Peak_COP"] = data_final2.Peak_EER_machine * data_final2.population * data_final2.Peak_Demand

                            data_final2["pop_DD"] = data_final2.population * data_final2.Peak_Demand
                            infos_peak = data_final2.groupby("NUTS_ID")["pop_Peak_COP"].sum() / data_final2.groupby("NUTS_ID")["pop_DD"].sum()

                            infos_peak = infos_peak.rename("Peak_COP").reset_index() if sized_for == "Heating" else infos_peak.rename("Peak_EER").reset_index()
                            final_info = final_info.merge(infos_peak)


                        final_info["LEVL"] = LEVL
                        list_NUTS.append(final_info.copy())
                    tmp = pd.concat(list_NUTS) ## Get one final dataframe
                    tmp.to_csv(Output_Folder + "NUTS/" + sized_for+ "/" +
                                        System.replace("/","") + "_" +
                                        Technology.replace("/","") + "_" +
                                        Mode + "_" +
                                        Emitters + ".csv")