import os
from epw import epw
from tqdm import tqdm

from Generic_HeatPump_Modeling.functions import *


'''

LAUNCHING THE SIMULATIONS AT ALL THE TMY POINTS

'''
### Defining pathes
Path_external_data = "PATH_TO_YOUR_DATA_FOLDER"
weather_data_path = Path_external_data + "Meteorological_data/TMY/input"

Data_Folder = "Models/HeatPump_models/data/"
Output_Folder = "Models/HeatPump_models/output/"

### Listing all the data points
list_points_meteo = os.listdir(weather_data_path)
list_points_meteo = [f for f in list_points_meteo if os.path.isdir(weather_data_path+'/'+f)]
list_files = []
for country in list_points_meteo:
    tmp_files = os.listdir(weather_data_path + "/" + country)
    list_files.append(list(set(["_".join(x.split("_")[0:3]) for x in tmp_files])))


## Loading the info on peak temperatures (T_base)
T_base_points = pd.read_csv(Data_Folder + "points_coordinates_with_Temps.csv")

## We load the different heating systems considered
Systems = pd.read_csv(Data_Folder + "simple_chauffage.csv", sep=";")
Systems = Systems.iloc[3:5, :] ## We keep only the inverter HPs

## We chose which degradation model to integrate (see functions for detail)
Models = {"Model_OnOff": "v2", "Model_Inverter": "v1", "Model_BiCompressor": "v1"}
## Settings of heating and cooling start and target temperatures
Settings = {"T_start_heating": 15, "T_target_heating": 20,
                  "T_start_cooling": 30, "T_target_cooling": 25,
                  "coeff_HC":0.165} #coeff_HC: coefficient linking the heating capacity and the fluid temperatures. Source: https://link.springer.com/article/10.1007/s12273-012-0089-0

### Listing all the systems and compatibility between emitters, modes etc.
all_emitters_lists = {"A/A HP": ["Fan coil unit"],
                      "A/W HP": ["FloorHeating", "RadiatorLT", "RadiatorMT", "RadiatorHT"],
                      "W/W HP": ["FloorHeating", "RadiatorLT", "RadiatorMT", "RadiatorHT"]}
compatible_cooling = {"Fan coil unit": True,
                      "FloorHeating": True,
                      "RadiatorLT": False, "RadiatorMT": False, "RadiatorHT": False}
WC_list = [True, False]
all_mode_lists = {"A/A HP": ["Monovalent"],
                  "A/W HP": ["Monovalent", "Bivalent parallel", "Bivalent alternative"],
                  "W/W HP": ["Monovalent", "Bivalent parallel", "Bivalent alternative"]}

reuse = False # Do we want to reuse the already saved data
Simulate_COP = True
Simulate_SCOP = False

a = epw()
k=15
i=0
### Looping on all the TMY points
for k in reversed(range(0,len(list_points_meteo))):
    if k==45: # Do not simulate Russia
        pass
    else:
        country = list_points_meteo[k]
        print(country)
        already_simulated = (os.path.exists(Output_Folder + "/SCOP_TMY/TMY_simulations_" + country + ".csv") if Simulate_SCOP else True) *\
                             (os.path.exists(Output_Folder + "/COP_TMY/TMY_simulations_" + country + ".csv") if Simulate_COP else True) * reuse
        if not already_simulated:
            files = list_files[k]
            file = "FRA_IF_Paris.MontSouris.071560" # Test value for debug
            list_SCOP = []
            list_COP = []
            for file in tqdm(files):
                try:
                    ### Load the weather file
                    if os.path.exists(weather_data_path + "/" + country + "/" + file + "_TMYx.epw"):
                        a.read(weather_data_path + "/" + country + "/" + file + "_TMYx.epw") ## All period if available, else smaller periodes below
                    elif os.path.exists(weather_data_path + "/" + country + "/" + file + "_TMYx.2007-2021.epw"):
                        a.read(weather_data_path + "/" + country + "/" + file + "_TMYx.2007-2021.epw")
                    elif os.path.exists(weather_data_path + "/" + country + "/" + file + "_TMYx.2004-2018.epw"):
                        a.read(weather_data_path + "/" + country + "/" + file + "_TMYx.2004-2018.epw")
                    else:
                        raise ValueError("No input")

                    ### Clean the weather data input
                    meteodata = a.dataframe
                    meteodata["date"] = pd.to_datetime(dict(year=2018,
                                                            month=meteodata.Month,
                                                            day=meteodata.Day,
                                                            hour=meteodata.Hour,
                                                            minute=meteodata.Minute))
                    meteodata = meteodata[["date", "Dry Bulb Temperature","Relative Humidity"]].rename(columns={"Dry Bulb Temperature":"temp",
                                                                                                                "Relative Humidity":"rh"})
                    meteodata = meteodata.set_index("date")

                    # Splitting name of the file to get the name of the station
                    tmp_name = file.split("_")
                    tmp_name[2] = ".".join(tmp_name[2].split(".")[:-1])
                    tmp_name.reverse()
                    name_station = " ".join(tmp_name)

                    ### Getting the peak temperature infos
                    T_base_h = T_base_points.loc[T_base_points.name==name_station,"T_base"].to_numpy()[0]
                    T_base_c = T_base_points.loc[T_base_points.name==name_station,"T_base_cooling"].to_numpy()[0]

                    for s in Systems.index:
                        System = Systems.loc[s, :]
                        emitters_list = all_emitters_lists[System.System]
                        mode_list = all_mode_lists[System.System]
                        WC = False if System["System"] == "A/A HP" else True ### Always consider weather compensation
                        for mode in mode_list:
                            eff_backup = 1 if mode in ["Monovalent", "Spatial backup"] else 0.9 #Electric heating has an efficiency of 1, gas boiler of 0.9
                            for emitters in emitters_list:
                                share_peak = 0.2 if mode in ["Monovalent", "Spatial backup"] else 0 # In monovalent, small backup for peak
                                share_energy = 0.1 if mode in ["Bivalent parallel", "Bivalent alternative"] else np.nan #In bivalent, the backup covers 10% of the demand

                                Consider_cooling = compatible_cooling[emitters] if T_base_c > Settings["T_target_cooling"] else False

                                ### Setting the inputs
                                configuration = {"Mode": mode,
                                                 "Emitters": emitters,
                                                 "Share_energy": share_energy,
                                                 "Share_peak": share_peak,  # for monovalent systems
                                                 "Weather_compensation": WC,
                                                 "Consider_cooling":Consider_cooling,
                                                 "T_base_h": T_base_h,
                                                 "T_peak_h": T_base_h,
                                                 "T_base_c": T_base_c,
                                                 "T_peak_c": T_base_c}

                                Simulation_PAC_input_parameter = {**Settings, **Models, **System.to_dict(),
                                                                  **configuration} # Merging the input

                                ### Setting the information in the df used as output
                                infos_df = pd.DataFrame(dict(Location=name_station,
                                                             System=Simulation_PAC_input_parameter["System"],
                                                             Technology=Simulation_PAC_input_parameter["Technology"],
                                                             Mode=Simulation_PAC_input_parameter["Mode"],
                                                             Emitters=Simulation_PAC_input_parameter["Emitters"],
                                                             Weather_compensation=Simulation_PAC_input_parameter[
                                                                 "Weather_compensation"],
                                                             Consider_PL=True), index=[i])

                                try:
                                    ### Launching the simulation when sized for heating
                                    Simulated_COP_sized_for_Heating = estim_SCOP(meteo_data=meteodata,
                                                               in_params=Simulation_PAC_input_parameter,
                                                               year=None,
                                                               consider_PL=True,
                                                               consider_defrost_equation=True,
                                                               oversizing_factor=1,
                                                               sized_for="Heating")

                                    ### Storing results
                                    infos_df_sized_for_Heating = infos_df.copy()
                                    if Simulate_SCOP:
                                        infos_df_sized_for_Heating["sized_for"]="Heating"
                                        infos_df_sized_for_Heating = extract_results(infos_df_sized_for_Heating,Simulated_COP_sized_for_Heating)

                                    if Simulate_COP:
                                        COP_df = np.round(Simulated_COP_sized_for_Heating["meteo_data"][["COP_machine_h","Backup_share"]],2)
                                        infos_df_rep = pd.DataFrame(np.repeat(infos_df.values, len(COP_df.index), axis=0),
                                                                    columns=infos_df.columns)
                                        infos_df_rep.reset_index(inplace=True)
                                        COP_data = pd.concat([COP_df, meteodata[["temp","rh"]]], axis=1).reset_index()
                                        COP_data = pd.merge(COP_data, infos_df_rep, left_index=True, right_index=True)
                                        COP_data = COP_data.drop(columns=["index"])

                                    ### If we consider cooling, we launch a new estimate for the sizing based on cooling
                                    infos_df_sized_for_Cooling = infos_df.copy()
                                    infos_df_sized_for_Cooling["sized_for"] = "Cooling"
                                    if Consider_cooling:
                                        Simulated_COP_sized_for_Cooling = estim_SCOP(meteo_data=meteodata,
                                                                                     in_params=Simulation_PAC_input_parameter,
                                                                                     year=None,
                                                                                     consider_PL=True,
                                                                                     consider_defrost_equation=True,
                                                                                     oversizing_factor=1,
                                                                                     sized_for="Cooling")
                                        ### Storing results
                                        if Simulate_SCOP:
                                            infos_df_sized_for_Cooling = infos_df.copy()
                                            infos_df_sized_for_Cooling["sized_for"] = "Cooling"
                                            infos_df_sized_for_Cooling = extract_results(infos_df_sized_for_Cooling,
                                                                                         Simulated_COP_sized_for_Cooling)

                                        if Simulate_COP:
                                            EER_df = np.round(Simulated_COP_sized_for_Cooling["meteo_data"][
                                                ["EER_machine_c"]],2)
                                            infos_df_rep = pd.DataFrame(
                                                np.repeat(infos_df.values, len(EER_df.index), axis=0),
                                                columns=infos_df.columns)
                                            infos_df_rep.reset_index(inplace=True)
                                            EER_data = pd.concat([EER_df, meteodata[["temp","rh"]]], axis=1).reset_index()
                                            EER_data = pd.merge(EER_data, infos_df_rep, left_index=True, right_index=True)
                                            EER_data = EER_data.drop(columns=["index"])
                                            data_COP_EER = COP_data.merge(EER_data)
                                    else:
                                        if Simulate_SCOP:
                                            infos_df_sized_for_Cooling = extract_results(infos_df_sized_for_Cooling, None)
                                        if Simulate_COP:
                                            data_COP_EER = COP_data.copy()
                                            data_COP_EER["EER_machine_c"] = np.nan
                                except:
                                    if Simulate_SCOP:
                                        infos_df_sized_for_Heating = infos_df.copy()
                                        infos_df_sized_for_Heating["sized_for"] = "Heating"
                                        infos_df_sized_for_Heating = extract_results(infos_df,None)
                                        infos_df_sized_for_Cooling = infos_df.copy()
                                        infos_df_sized_for_Cooling["sized_for"] = "Cooling"
                                        infos_df_sized_for_Cooling = extract_results(infos_df,None)
                                if Simulate_SCOP:
                                    list_SCOP.append(infos_df_sized_for_Heating)
                                    list_SCOP.append(infos_df_sized_for_Cooling)
                                if Simulate_COP:
                                    list_COP.append(data_COP_EER)
                                i = i + 1
                except:
                    pass
            if Simulate_COP:
                final_COP = pd.concat(list_COP).rename(columns={"Location": "name"})
                test = pd.melt(final_COP, id_vars=['date', 'name', 'System', 'Technology', 'Mode',
                                                   'Emitters', 'Weather_compensation',
                                                   'Consider_PL'],
                               value_vars=['temp', "rh", 'COP_machine_h', "Backup_share", "EER_machine_c"])
                final_COP = pd.pivot_table(test, index=['name', 'System', 'Technology', 'Mode',
                                                              'Emitters', 'Weather_compensation',
                                                              'Consider_PL', 'variable'],
                                                 values=['value'],
                                                 columns=['date'], aggfunc="sum").reset_index()
                final_COP.columns = [*['name', 'System', 'Technology', 'Mode',
                                             'Emitters', 'Weather_compensation',
                                             'Consider_PL', 'variable'], *meteodata.index]
                final_COP.to_csv(Output_Folder + "/COP_TMY/TMY_simulations_" + country + ".csv",index=False)
            if Simulate_SCOP:
                tmp_data = pd.concat(list_SCOP).rename(columns={"Location":"name"}).merge(T_base_points,left_on="name",right_on="name")
                tmp_data.to_csv(Output_Folder + "/SCOP_TMY/TMY_simulations_" + country + ".csv",index=False)

all_SCOP=[]
for k in reversed(range(0, len(list_points_meteo))):
    country = list_points_meteo[k]
    tmp_data = pd.read_csv(Output_Folder + "/SCOP_TMY/TMY_simulations_" + country + ".csv")  # Load exisiting data
    all_SCOP.append(tmp_data)
all_SCOP = pd.concat(all_SCOP)


### Keeping only the information of interest
info_test = all_SCOP[["name","System","Technology","Mode","Emitters","sized_for",
                      "SCOP_machine","SCOP_tot","SEER_machine","APF_machine","APF_tot",
                      "Peak_COP_machine","Peak_COP_tot","Peak_EER_machine",
                      "Energy_share","Peak_share","HDD","CDD"]]

info_test.to_csv(Output_Folder + "/TMY_simulations.csv",index=False)
