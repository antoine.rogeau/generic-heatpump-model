import pandas as pd
import numpy as np
import xarray as xr
import os
from itertools import product
import geopandas as gpd

from tfinterpy.krige import OK
from tfinterpy.variogram import calculateDefaultVariogram3D

'''

LAUNCHING THE KRIGING ON SCOP and peak data (SCOP, SEER, Peak COP and EER, energy shares and peak shares)

'''
### Defining pathes
Data_Folder = "Models/HeatPump_models/data/"
Path_external_data = "PATH_TO_YOUR_DATA_FOLDER"
Output_Folder = "Models/HeatPump_models/output/"

### Loading the borders shapefile
New_Europe = gpd.read_file(Data_Folder + "countries_border.shp")
New_Europe_3035 = New_Europe.to_crs(3035)

minx, miny, maxx, maxy = New_Europe_3035.geometry.total_bounds # Get the bounding box

data_pop = xr.open_dataset(Path_external_data + "Maps/JRC/POPULATION/JRC_1K_POP_2018.tif") #download here: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat
data_pop = data_pop.sel(band=1)
data_pop = data_pop.rename({"band_data": "population"})

### We aggregate the df at 5x5km resolution for a faster kriging
data_pop = data_pop.coarsen(x=3,boundary="pad").sum(skipna=True).coarsen(y=5,boundary="pad").sum(skipna=True)

data_pop_df = data_pop.to_dataframe().reset_index()
data_pop_df.loc[data_pop_df["population"]==0,"population"] = np.nan

### Get the resolution info
resx = data_pop_df['x'].unique()[1] - data_pop_df['x'].unique()[0]
resy = data_pop_df['y'].unique()[1] - data_pop_df['y'].unique()[0]
index_x = [*range(int(data_pop_df["x"].min()),int(maxx),int(resx))]
index_y = [*range(int(data_pop_df["y"].min()),int(maxy),int(-resy))]

MI_xy = pd.MultiIndex.from_product([index_x,index_y],names=['x', 'y']) # Generate multiindex
data_pop_df = data_pop_df.set_index(["x","y"])
data_pop_df = data_pop_df.reindex(MI_xy)

### Generate final template xarray
data_pop = data_pop_df.to_xarray().transpose()
data_pop = data_pop.rio.write_crs('EPSG:3035')

### Projecting the population raster to WGS84 reference system
data_pop_WGS84 = data_pop.rio.reproject('EPSG:4326').drop_vars("band")
lat = data_pop_WGS84.coords['y'].values
lon = data_pop_WGS84.coords['x'].values

### Loading the elevation data (in tiles format) and merge
### The data can be downloaded here: https://portal.opentopography.org/raster?opentopoID=OTSDEM.032021.4326.1
list_data_elev = []
for x, y in product(range(1, 4), repeat=2):
    data_elev = xr.open_dataset(
        Path_external_data + "Maps/Copernicus/eu-dem/output_COP90_" + str(x) + "_" + str(
            y) + ".tif")
    data_elev = data_elev.interp_like(data_pop_WGS84) #Align with the population xarray in WGS84 projection
    list_data_elev.append(data_elev)

#Merge all data
data_elev_WGS84 = xr.merge(list_data_elev)
data_elev_WGS84 = data_elev_WGS84.dropna(dim="x",how="all")
data_elev_WGS84 = data_elev_WGS84.dropna(dim="y",how="all")
data_elev = data_elev_WGS84.rio.reproject_match(data_pop,nodata=np.nan)

### Loading the data of estimated performances, which include Degree days and Base temperatures
all_data_point = pd.read_csv(Output_Folder + "/TMY_simulations.csv")

### Defining a single configuration for debug runs
sized_for = "Cooling"
System = "A/A HP"
Technology = "Inverter"
Mode = "Monovalent"
Emitters = 'Fan coil unit'

reuse = False ### Boolean to re-run the simulation if the save file already exists

### Looping on all the consfigurations
for sized_for in ["Heating", "Cooling"]:
    tmp_all_data_point = all_data_point[all_data_point["sized_for"] == sized_for]
    tmp_all_data_point = tmp_all_data_point[~np.isnan(tmp_all_data_point["SCOP_machine"])]
    if sized_for == "Cooling":
        tmp_all_data_point = tmp_all_data_point[all_data_point.Mode == "Monovalent"]
    for System in tmp_all_data_point.System.unique():
        tmp_point_1 = tmp_all_data_point[tmp_all_data_point.System == System]
        for Technology in tmp_point_1.Technology.unique():
            tmp_point_2 = tmp_point_1[tmp_point_1.Technology == Technology]
            for Mode in tmp_point_2.Mode.unique():
                tmp_point_3 = tmp_point_2[tmp_point_2.Mode == Mode]
                for Emitters in tmp_point_3.Emitters.unique():
                    ### defining the output file
                    simulated = System.replace("/", "") + "_" +Technology.replace("/", "") + "_" +Mode + "_" +Emitters + "_sizedfor_" +sized_for
                    file_name = Output_Folder + "Maps/" +  simulated + ".nc"
                    print(simulated)
                    if (not os.path.exists(file_name)) or (reuse == False):
                        tmp_point_4 = tmp_point_3[tmp_point_3.Emitters == Emitters]
                        tmp_point_4 = tmp_point_4[["SCOP_machine", "SEER_machine",
                                                   "Peak_COP_machine", "Peak_EER_machine",
                                                   "Energy_share", "Peak_share", "lon", "lat"]] ### We keep the variables of interest

                        tmp_point_4 = tmp_point_4[~np.isnan(tmp_point_4["SCOP_machine"])] if sized_for == "Heating" else \
                        tmp_point_4[~np.isnan(tmp_point_4["SEER_machine"])] ## Keeping the non-NA observations

                        # Removing multiple points
                        tmp_point_4 = tmp_point_4.drop_duplicates(["lon", "lat"])
                        points = np.array([[tmp_point_4.iloc[x, -2], tmp_point_4.iloc[x, -1]] for x in
                                  range(0, len(tmp_point_4))]) # Convert to array

                        ### Generating a geodataframe (points)
                        gdf_points = gpd.GeoDataFrame(
                            points, geometry=gpd.points_from_xy(points[:,0], points[:,1]), crs="EPSG:4326"
                        )
                        gdf_points.to_crs(3035,inplace=True) # Projection
                        points_LBT93 = gdf_points.get_coordinates()

                        points_LBT93 = np.array(points_LBT93)
                        points_with_elev = [
                            [*x, data_elev.band_data.sel(x=x[0], y=x[1], method="nearest").to_numpy()[0]] for x in
                            points_LBT93] ### get the elevation information near the points
                        points_with_elev = pd.DataFrame(points_with_elev).to_numpy()

                        # We are going to loop over all the indicators to do all the kriging
                        to_loop = tmp_point_4.columns.values.tolist()
                        to_loop = [ele for ele in to_loop if ele not in ["lat", "lon"]]
                        if Mode != "Bivalent parallel":
                            to_loop = to_loop[:-1] ## If bivalent parallel, we are also interested in the peak share

                        list_maps=[]
                        for l in to_loop:
                            values = np.array([tmp_point_4[l]])
                            if not np.isnan(values).all():
                                df_values = np.concatenate((points_with_elev, values.T), axis=1)
                                df_values = pd.DataFrame(df_values, columns=["X", "Y", "Z", l])
                                df_values.replace([np.inf, -np.inf], np.nan, inplace=True)
                                df_values = df_values.dropna()
                                if len(df_values) > 0:
                                    df_values["X"] = df_values["X"] / 1e3 ### To get the same order of magnitude of altitude and x,y, we devide lon and lat by 1000
                                    df_values["Y"] = df_values["Y"] / 1e3 ### To get the same order of magnitude of altitude and x,y, we devide lon and lat by 1000
                                    values = df_values[["X", "Y", "Z", l]].values

                                    exe = OK(values, '3d')  # Create a ok interpolator.
                                    vb = calculateDefaultVariogram3D(values)  # Calculate a default variogram function.
                                    # plt.figure()
                                    # vb.showVariogram()
                                    # plt.show()

                                    ### We get all the new points where we want to interpolate the data
                                    data_elev_tmp = data_elev.to_dataframe()
                                    data_elev_tmp = data_elev_tmp.reset_index()
                                    data_elev_tmp = data_elev_tmp.rename(columns={"band_data": "z"})[["x","y","z"]]
                                    data_elev_tmp["x"] = data_elev_tmp["x"] / 1e3 ### To get the same order of magnitude of altitude and x,y, we devide lon and lat by 1000
                                    data_elev_tmp["y"] = data_elev_tmp["y"] / 1e3 ### To get the same order of magnitude of altitude and x,y, we devide lon and lat by 1000
                                    data_elev_tmp = data_elev_tmp.dropna().to_numpy()

                                    ### EXECUTE THE KRIGING
                                    pro, sigma = exe.execute(data_elev_tmp, 10, vb.getVariogram())
                                    points_with_SCOP = np.column_stack((data_elev_tmp,pro,sigma))
                                    points_with_SCOP2 = np.delete(points_with_SCOP, 2, 1)

                                    ## We generate the datadrame with lon, lat, value and standard error
                                    points_with_SCOP2 = pd.DataFrame(points_with_SCOP2,columns=["x","y",l,"SE_"+l])
                                    points_with_SCOP2["x"] = points_with_SCOP2["x"] * 1e3
                                    points_with_SCOP2["y"] = points_with_SCOP2["y"] * 1e3
                                    dataset = points_with_SCOP2.set_index(['x', 'y']).to_xarray() ## And we finally convert back to xarray
                                    dataset.rio.write_crs("EPSG:3035", inplace=True)

                                    ### We clip on the continental area (removing values out of the coastiles)
                                    test = dataset.rio.clip(New_Europe_3035.geometry.values, New_Europe_3035.crs)

                                    ### We smooth a bit the data to (a) remove some erroneous NA values and (b) avoid some peaks
                                    window_size = 3
                                    smoothed_test = test.rolling(x=window_size, center=True,min_periods=2).mean(skipna=True).rolling(y=window_size,
                                                                                                        center=True,min_periods=2).mean(skipna=True)

                                    smoothed_test = smoothed_test.transpose().rio.reproject('EPSG:4326') #Reprojecting to WGS84
                                    smoothed_test = smoothed_test.transpose()
                                    list_maps.append(smoothed_test)

                        ### Merging all the data, projecting and saving
                        final_map = xr.merge(list_maps)
                        final_map = final_map.transpose()
                        final_map = final_map.rio.reproject('EPSG:4326')

                        final_map.to_netcdf(file_name, engine='netcdf4')