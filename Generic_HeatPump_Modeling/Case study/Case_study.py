from epw import epw
from tqdm import tqdm

from functions.f_heat_pump import *

'''

LAUNCHING THE SIMULATIONS FOR THE CASE STUDY

'''
### Defining pathes
Path_external_data = "PATH_TO_YOUR_DATA_FOLDER"
weather_data_path = Path_external_data + "/Meteorological_data/TMY/input" # Download here: https://climate.onebuilding.org/WMO_Region_6_Europe/default.html
Output_Folder = "Models/HeatPump_models/output/"

Data_Folder = "Models/HeatPump_models/data/"

## Loading the info on peak temperatures (T_base)
T_base_points = pd.read_csv(Data_Folder + "points_coordinates_with_Temps.csv")

## We load the different heating systems considered
Systems = pd.read_csv(Data_Folder + "simple_chauffage.csv", sep=";")

## We chose which degradation model to integrate (see functions for detail)
Models = {"Model_OnOff": "v2", "Model_Inverter": "v1", "Model_BiCompressor": "v1"}
## Settings of heating and cooling start and target temperatures
Settings = {"T_start_heating": 15, "T_target_heating": 20,
                  "T_start_cooling": 30, "T_target_cooling": 25,
                  "coeff_HC":0.165} #coeff_HC: coefficient linking the heating capacity and the fluid temperatures. Source: https://link.springer.com/article/10.1007/s12273-012-0089-0


a = epw()

### The studycase is based on Paris weather data
country = "France"
file = "FRA_IF_Paris.MontSouris.071560"
a.read(weather_data_path + "/" + country + "/" + file + "_TMYx.2007-2021.epw") #Loading dataset
meteodata = a.dataframe

meteodata["date"] = pd.to_datetime(dict(year=2018,
                                        month=meteodata.Month,
                                        day=meteodata.Day,
                                        hour=meteodata.Hour,
                                        minute=meteodata.Minute))
meteodata = meteodata[["date", "Dry Bulb Temperature","Relative Humidity"]].rename(columns={"Dry Bulb Temperature":"temp",
                                                                                                              "Relative Humidity":"rh"})
meteodata = meteodata.set_index("date")

#Splitting name of the file to get the name of the station
tmp_name = file.split("_")
tmp_name[2] = ".".join(tmp_name[2].split(".")[:-1])
tmp_name.reverse()
name_station = " ".join(tmp_name)

### Getting the peak temperature infos
T_base_h = T_base_points.loc[T_base_points.name==name_station,"T_base"].to_numpy()[0]
T_base_c = T_base_points.loc[T_base_points.name==name_station,"T_base_cooling"].to_numpy()[0]
Systems = Systems[(Systems["Technology"] == "Inverter") & (Systems["System"] != "W/W HP")] # We don't consider Ground source heat pumps


### Listing all the systems and compatibility between emitters, modes etc. considered in the study case
all_emitters_lists = {"A/A HP": ["Fan coil unit"],
                      "A/W HP": ["RadiatorHT"]}
all_mode_lists = {"A/A HP": ["Spatial backup"],
                  "A/W HP": ["Monovalent", "Bivalent parallel", "Bivalent alternative"]}

i = 0
list_SCOP = []
list_COP = []
#Looping between Base case (1) Mild retrofit (2) and Deep retrofit (4)
for OS in [1,2,4]:
    for s in tqdm(Systems.index):
        System = Systems.loc[s, :]
        emitters_list = all_emitters_lists[System.System]
        mode_list = all_mode_lists[System.System]
        WC=False if System["System"] == "A/A HP" else True #We always consider weather compensation for A/W HP
        for mode in mode_list:
            eff_backup = 1 if mode in ["Monovalent", "Spatial backup"] else 0.9 #Electric heating has an efficiency of 1, gas boiler of 0.9
            for emitters in emitters_list:
                share_peak = 0.2 if mode in ["Monovalent", "Spatial backup"] else 0 # In monovalent, small backup for peak
                share_energy = 0.1 if mode in ["Bivalent parallel", "Bivalent alternative"] else np.nan #In bivalent, the backup covers 10% of the demand

                ### Setting the inputs
                configuration = {"Mode": mode,
                                 "Emitters": emitters,
                                 "Share_energy": share_energy,
                                 "Share_peak": share_peak,  # for monovalent systems
                                 "Weather_compensation": WC,
                                 "Consider_cooling": False,
                                 "T_base_h": T_base_h,
                                 "T_peak_h": T_base_h,
                                 "T_base_c": T_base_c,
                                 "T_peak_c": T_base_c}

                Simulation_PAC_input_parameter = {**Settings, **Models, **System.to_dict(), **configuration} # Merging the input
                Bools = [False] if System["System"] == "A/A HP" else [False,True] # Booleans to adjust (or not) the fluid temperature
                for Bool in Bools:
                    ### Setting the information in the df used as output
                    infos_df = pd.DataFrame(dict(Location=name_station,
                                                 System=Simulation_PAC_input_parameter["System"],
                                                 Technology=Simulation_PAC_input_parameter["Technology"],
                                                 Mode=Simulation_PAC_input_parameter["Mode"],
                                                 Emitters=Simulation_PAC_input_parameter["Emitters"],
                                                 Weather_compensation=Simulation_PAC_input_parameter[
                                                     "Weather_compensation"],
                                                 Oversizing=OS,
                                                 Adjust_Tfluid=Bool,
                                                 Order="Wrong"), index=[i]) # The order of the retrofit is "Wrong": Thermal retrofit after HP installation

                    ### Launching the simulation
                    Simulated_COP = estim_SCOP(meteo_data=meteodata,
                                               in_params=Simulation_PAC_input_parameter,
                                               year=2018,
                                               consider_PL=True,
                                               oversizing_factor=OS,force_bivalent=not Bool,
                                               adjust_T_fluid=Bool)

                    ### Storing results
                    results = Simulated_COP.copy()
                    results = extract_results(results,Simulated_COP)
                    infos_df["SCOP"] = results["SCOP_tot"]
                    infos_df["Pnom_elec"] = results["Pelec_nom"]
                    infos_df["HCnom"] = results["HC_nom"]
                    infos_df["Energy_share"] = results["RE"]
                    infos_df["Peak_COP"] = results["Peak_COP_tot"]
                    infos_df["Peak_share"] = results["Peak_share"]
                    infos_df["Energy"] = results["meteo_data"]["Q_h_OS"].sum()
                    list_SCOP.append(infos_df.copy())
                    i = i+1

                    ### If the OS >1 (the building has been retrofitted) we rerun the simulation with adapted temperature
                    ### For the right order with reduced water regime
                    condition = (Bool and OS > 1) if s==4 else ((not Bool) and OS > 1)

                    if condition:
                        comp_params = Simulated_COP["comp_params"]
                        new_tfluid = comp_params["a_h"] * comp_params["T_base_h"] +comp_params["b_h"] # We adapt the fluid temperature
                        configuration = {"Mode": mode,
                                         "Emitters": emitters,
                                         "Share_energy": share_energy,
                                         "Share_peak": share_peak,  # for monovalent systems
                                         "Weather_compensation": WC,
                                         "Consider_cooling": False,
                                         "T_base_h": T_base_h,
                                         "T_peak_h": T_base_h,
                                         "T_base_c": T_base_c,
                                         "T_peak_c": T_base_c,
                                         "T_fluid_h":new_tfluid, #Forcing the new water temperature
                                         "T_fluid_c":21} # No importance here

                        Simulation_PAC_input_parameter = {**Settings, **Models, **System.to_dict(),
                                                          **configuration}

                        ### Launching the simulation
                        Simulated_COP = estim_SCOP(meteo_data=meteodata,
                                                   in_params=Simulation_PAC_input_parameter,
                                                   year=2018,
                                                   consider_PL=True,
                                                   oversizing_factor=1,force_bivalent=False,
                                                   adjust_T_fluid=Bool)

                        ### Storing results
                        infos_df["Order"] = "Right"
                        results = Simulated_COP.copy()
                        results = extract_results(results, Simulated_COP)
                        infos_df["SCOP"] = results["SCOP_tot"]
                        infos_df["Pnom_elec"] = results["Pelec_nom"] / OS
                        infos_df["HCnom"] = results["HC_nom"] / OS
                        infos_df["Energy_share"] = results["RE"]
                        infos_df["Peak_COP"] = results["Peak_COP_tot"]
                        infos_df["Peak_share"] = results["Peak_share"]
                        infos_df["Energy"] = results["meteo_data"]["Q_h_OS"].sum() / OS

                        list_SCOP.append(infos_df.copy())
                        i = i + 1

list_SCOP = pd.concat(list_SCOP)
list_SCOP.to_csv(Output_Folder + "Study_case_KPIs.csv")